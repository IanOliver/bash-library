#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="${MACHINE_NAME} DDNS"

CLOUDFLARE_DDNS "topleveldomain.com" "record.topleveldomain.com" "600" "token" "XXXXXXXXXXXXXXXXXXXXXXX"
