#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

sleep 10

PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "" "${MACHINE_NAME} on"
