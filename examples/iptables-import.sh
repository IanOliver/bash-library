#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="${MACHINE_NAME} iptables Import"

REQUIRE_ROOT

# Import the IPv4 rules
sudo iptables-restore < ${IPTABLES_IPV4_FILE}
