#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

SYNCTHING_CERT_UPDATE_LOCAL "syncthing.domain.com" "/path/to/certificates/syncthing" "/home/username/.config/syncthing"

