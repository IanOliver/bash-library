#!/usr/bin/env bash

if [[ -z "${MACHINE_SCRIPTS_DIR}" ]]; then
	# Get full script directory path
	MACHINE_SCRIPTS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fi

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="Update Allowlists (Exact)"
echo "Starting: ${OPERATION}"

PI_HOLE_UPDATE_SUBSCRIPTIONS "allowlist-exact" "${ALLOWLISTS_EXACT[@]}"

echo "${OPERATION} complete!"
