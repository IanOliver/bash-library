#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="${MACHINE_NAME} iptables Edit"

REQUIRE_ROOT

sudo ${EDITOR} ${IPTABLES_IPV4_FILE}
