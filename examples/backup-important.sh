#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

SCRIPT_LOCK="${MACHINE_SCRIPTS_DIR}/lock/backup"

SCRIPT_LOCK_START "${SCRIPT_LOCK}"

CLEANUP_RDIFF "1M" "/path/to/backup/"
BACKUP_RDIFF "/path/to/source/" "/path/to/backup/"

SCRIPT_LOCK_FINISH "${SCRIPT_LOCK}"
