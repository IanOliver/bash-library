#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="${MACHINE_NAME} checking drive usage"

DRIVE_USAGE     "/"				"65"
DRIVE_USAGE     "/backup"		"70"
DRIVE_USAGE     "/core-backup"	"70"
DRIVE_USAGE     "/mnt/point"	"70"
