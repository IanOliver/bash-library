#!/usr/bin/env bash

MACHINE_NAME=""
NETWORK_NAME=""
MACHINE_DEFAULT_USER=""
MACHINE_ARCHITECTURE="" # E.g. "amd64" or "arm64"

SCRIPT_LOGS_DIR="${MACHINE_SCRIPTS_DIR}/log"
SCRIPT_LOCKS_DIR="${MACHINE_SCRIPTS_DIR}/lock"

# pfSense
PFSENSE_CERT_USER=""
PFSENSE_IP=""

# Syncthing
SYNCTHING_API_KEY=""

# Pushover
PUSHOVER_DEFAULT_USER=""
PUSHOVER_DEFAULT_TOKEN=""

# Mailgun
MAILGUN_URL="" # E.g. "https://api.eu.mailgun.net/v3/domain.com"
MAILGUN_API_KEY=""
MAILGUN_DEFAULT_SENDING_NAME=""
MAILGUN_DEFAULT_SENDING_ADDRESS="" # E.g. "name@domain.com"
MAILGUN_DEFAULT_RECEIVING_NAME=""
MAILGUN_DEFAULT_RECEIVING_ADDRESS=""

# Twilio
TWILIO_DEFAULT_ACCOUNT_SID=""
TWILIO_DEFAULT_AUTH_TOKEN=""
TWILIO_DEFAULT_SENDING_NUMBER="" # E.g. "+44XXXXYYYYYY"
TWILIO_DEFAULT_RECEIVING_NUMBER="" # E.g. "+44XXXXYYYYYY"

# iptables
IPTABLES_IPV4_FILE="iptables-ipv4.rules"
IPTABLES_IPV6_FILE=""

# Text editor
EDITOR="nano" # E.g. nano, subl

# Pi-hole
PI_HOLE_DOWNLOAD_DIR="/home/pi/pi-hole/downloads"
PI_HOLE_LIST_UPDATE_LOCK="${SCRIPT_LOCKS_DIR}/pi-hole-list-update"
PI_HOLE_CLOUDFLARE_TEAMS_SUBDOMAIN=""

# If domains need to be allowlisted prior to downloading their hosted lists
# pihole -w domain.com

# Exact allowlists
ALLOWLISTS_EXACT+=("https://domain.com/allowlists/exact/category1")
ALLOWLISTS_EXACT+=("https://domain.com/allowlists/exact/category2")


# Regex allowlists
ALLOWLISTS_REGEX+=("https://domain.com/allowlists/regex/category1")
ALLOWLISTS_REGEX+=("https://domain.com/allowlists/regex/category2")


# Regex blocklists
BLOCKLISTS_REGEX+=("https://domain.com/blocklists/regex/category1")
BLOCKLISTS_REGEX+=("https://domain.com/blocklists/regex/category2")


# Blocklists
BLOCKLISTS+=("https://domain.com/blocklists/all")
