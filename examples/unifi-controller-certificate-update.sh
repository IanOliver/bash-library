#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

UNIFI_CONTROLLER_CERT_UPDATE_LOCAL "unifi.domain.com" "/path/to/certificates/unifi-controller"

