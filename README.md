# Bash Library

## Download

	mkdir -p "$HOME/scripts/bash-library"
	git clone https://IanOliver@bitbucket.org/IanOliver/bash-library.git "$HOME/scripts/bash-library"
	
## Setup

	crontab -e

Add

	MACHINE_SCRIPTS_DIR="/home/user/scripts"
	
Then

	sudo nano /etc/profile
	
Add

	MACHINE_SCRIPTS_DIR="/home/user/scripts"
	
Copy example variables file

	cp "$HOME/scripts/bash-library/examples/this-machine.sh" "$HOME/scripts/this-machine.sh"
	nano "$HOME/scripts/this-machine.sh"
	
Add variables and arrays where required and as needed.

Copy desired example scripts to scripts/. and edit as needed.

	cp "$HOME/scripts/bash-library/examples/update-allowlists-regex.sh" scripts/.
	cp "$HOME/scripts/bash-library/examples/update-allowlists-exact.sh" scripts/.
	cp "$HOME/scripts/bash-library/examples/update-blocklists-regex.sh" scripts/.

## Run scripts

	bash scripts/update-allowlists-regex.sh
	bash scripts/update-allowlists-exact.sh
	bash scripts/update-blocklists-regex.sh
	
## Pi-hole Cron
	0 2 1 * * sudo bash /path/to/scripts/update-blocklists.sh
	30 3 * * * sudo bash /path/to/scripts/update-blocklists-regex.sh
	0,12,24,36,48 6-20 * * * sudo bash /path/to/scripts/update-allowlists-exact.sh
	6,18,30,42,54 6-20 * * * sudo bash /path/to/scripts/update-allowlists-regex.sh
