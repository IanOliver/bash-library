#!/usr/bin/env bash

DNS_LOOKUP () {
	
	# Function to perform a simple DNS lookup.
	
	# Expected output:
	# A simple IP address, or list of IP addresses, e.g.:
	# 216.58.206.142
	# or
	# 104.26.2.79
	# 104.26.3.79
	
	# Input arguments:
	# $1 = Domain name to lookup
	# $2 = (Optional) DNS resolver to use
	
	DOMAIN_NAME=${1}
	DNS_RESOLVER=${2}
	
	# Example usage:
	# DNS_LOOKUP google.com
	# DNS_LOOKUP google.com 192.168.1.250
	# DNS_LOOKUP google.com 1.1.1.1
	
	if [[ ! -z "${DNS_RESOLVER}" ]]; then
		dig @"${DNS_RESOLVER}" "${DOMAIN_NAME}" +short
	else
		dig "${DOMAIN_NAME}" +short
	fi
}

CHECK_DNS_RESOLVER () {
	
	# Function to check that a DNS resolver is providing the expected response.
	
	# Resource:
	# https://serverfault.com/questions/372066/force-dig-to-resolve-without-using-cache
	
	# Expected output:
	# 
	
	# Input arguments:
	# $1 = DNS resolver IP address to test
	# $2 = DNS resolver name - used for human-friendly reference
	# $3 = Type of record to query (e.g. A, TXT, CNAME, MX)
	# $4 = DNS record name
	# $4 = The expected value of the DNS record
	
	DNS_RESOLVER_IP=${1}
	DNS_RESOLVER_NAME=${2}
	DNS_RECORD_TYPE=${3}
	DNS_RECORD_NAME=${4}
	EXPECTED_RECORD_VALUE=${5}
	
	# Example usage:
	# CHECK_DNS_RESOLVER "192.168.1.250" "Primary Pi-hole" "A" "google.com" ""
	# CHECK_DNS_RESOLVER "1.1.1.1" "Cloudflare" "TXT" "something.domainname.com" "jfe4njdmyn3wyw59v4z3938g9wk"
	
	RESPONSE=$(dig @"${DNS_RESOLVER_IP}" "${DNS_RECORD_TYPE}" "${DNS_RECORD_NAME}" +short)
	
	# Remove any double quotes - these (by default) are inserted before and after TXT records by dig
	RESPONSE=${RESPONSE//\"/}
	
	if [[ "${RESPONSE}" = "${EXPECTED_RECORD_VALUE}" ]]; then
		echo "Expected result! Resolver ${DNS_RESOLVER_IP} (${DNS_RESOLVER_NAME}) is currently providing ${RESPONSE} as the value for ${DNS_RECORD_TYPE}/${DNS_RECORD_NAME}"
	else
		echo "Unexpected result! Resolver ${DNS_RESOLVER_IP} (${DNS_RESOLVER_NAME}) is currently providing ${RESPONSE} as the value for ${DNS_RECORD_TYPE}/${DNS_RECORD_NAME}, but the result should be ${EXPECTED_RECORD_VALUE}."
		
		# Alert user
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "DNS Resolution Error" "Message text"
	fi
	
	

}


DNS_LOOKUP_LOG () {
	
	# Function to perform a DNS lookup and log the result, if different. Changes will trigger an alert.
	
	# Input arguments:
	# $1 = Domain name to lookup
	# $2 = (Optional) Nickname of domain (e.g. Bob's Home)
	# $3 = (Optional) DNS resolver to use
	
	# Example usage:
	# DNS_LOOKUP_LOG ddns.domain.com
	# DNS_LOOKUP_LOG ddns.domain.com 192.168.1.250
	# DNS_LOOKUP_LOG ddns.domain.com 1.1.1.1
	
	
	local DNS_DOMAIN_NAME=${1}
	local DNS_NICKNAME=${2}
	local DNS_RESOLVER=${3}

	local DATE_AND_TIME_LOG="$(TIMESTAMP DATE_NOW) $(TIMESTAMP TIME_NOW_FULL)"
	local STORAGE_DIRECTORY="${SCRIPT_LOGS_DIR}/dns/${DNS_DOMAIN_NAME}"
	local PREVIOUS_DNS_FILE="${STORAGE_DIRECTORY}/dns_previous.txt"
	local HISTORY_FILE="${STORAGE_DIRECTORY}/history.txt"
	
	# LOGGER
	LOG() {
		if [[ "${1}" ]]; then
			
			local IP=${1}
			
			# Append to history file
			echo -e "[$DATE_AND_TIME_LOG]: IP changed to ${IP}" >> $HISTORY_FILE
			
			# Overwrite previous file
			echo "${IP}" > "${PREVIOUS_DNS_FILE}"
		fi
	}
	
	
	# Testing
	#echo "DATE_AND_TIME_LOG: ${DATE_AND_TIME_LOG}"
	#echo "STORAGE_DIRECTORY: ${STORAGE_DIRECTORY}"
	#echo "PREVIOUS_DNS_FILE: ${PREVIOUS_DNS_FILE}"
	#echo "HISTORY_FILE: ${HISTORY_FILE}"
	
	echo "Performing lookup for ${DNS_DOMAIN_NAME}"
	
	# Perform the lookup
	if [[ ! -z "${DNS_RESOLVER}" ]]; then
		local LOOKUP_IP="$(dig @"${DNS_RESOLVER}" "${DNS_DOMAIN_NAME}" +short)"
	else
		local LOOKUP_IP="$(dig "${DNS_DOMAIN_NAME}" +short)"
	fi
	
	
	# Display the fresh lookup IP
	echo "Lookup IP: ${LOOKUP_IP}"
	
	
	
	# Is this the first time the script has run?
	if [[ ! -d "${STORAGE_DIRECTORY}" ]] || [[ ! -f "${PREVIOUS_DNS_FILE}" ]] || [[ ! -f "${HISTORY_FILE}" ]]; then
		
		# The log and history files do exist: it's the first run
		local FIRST_RUN=true
		echo "This is the first run: making relevant directories and files, logging first entries"
		
		# Make directory
		mkdir -p "${STORAGE_DIRECTORY}"
		
		# Make history file
		touch "${HISTORY_FILE}"
		
		# Make previous file
		touch "${PREVIOUS_DNS_FILE}"
		
		# Log the first IP
		LOG "${LOOKUP_IP}"
	
	# It isn't the first run	
	else
		
		# The log and history files already exist: it's not the first run
		local FIRST_RUN=false
		
		local PREVIOUS_IP="$(cat ${PREVIOUS_DNS_FILE})"
		echo "Previous IP: ${PREVIOUS_IP}"
		
		if [[ "${PREVIOUS_IP}" == "${LOOKUP_IP}" ]]; then
			
			# The IPs are the same
			echo "Previous IP and lookup IP are the same (${PREVIOUS_IP})"
			
		else
			
			# The IP has changed
			echo "Previous IP and lookup IP are different"
			
			# Log it to history
			LOG "${LOOKUP_IP}"
			
			local MESSAGE="${DNS_NICKNAME} (${DNS_DOMAIN_NAME}) IP changed to ${LOOKUP_IP}. It was previously ${PREVIOUS_IP}."
			
			# Pushover alert (if variables exist)
			if [[ ! -z "${PUSHOVER_DEFAULT_USER}" ]] && [[ ! -z "${PUSHOVER_DEFAULT_TOKEN}" ]]; then
				PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "" "${MESSAGE}"	
			fi
			
			# Mailgun alert (if variables exist)
			if [[ ! -z "${MAILGUN_URL}" ]] && [[ ! -z "${MAILGUN_API_KEY}" ]] && [[ ! -z "${MAILGUN_DEFAULT_SENDING_ADDRESS}" ]] && [[ ! -z "${MAILGUN_DEFAULT_RECEIVING_ADDRESS}" ]]; then
				MAILGUN	"${MAILGUN_DEFAULT_RECEIVING_NAME}" "${MAILGUN_DEFAULT_RECEIVING_ADDRESS}" "${MAILGUN_DEFAULT_SENDING_NAME}" "${MAILGUN_DEFAULT_SENDING_ADDRESS}" "" "" "" "" "${DNS_NICKNAME} DNS IP Changed" "${MESSAGE}" "" ""
			fi

		fi
		
		
	fi
	
}



CLOUDFLARE_DDNS() {
	
	# Function to perform DDNS with Cloudflare's API and a given DNS record.
	
	# Requires curl to be installed
	
	# Input arguments:
	# $1 = Domain zone name (e.g. domain.com)
	# $2 = Record name (e.g. my-ddns.domain.com)
	# $3 = Record TTL (e.g. 300; default is 600 if unspecified)
	# $4 = 'token' or 'key+email' - are we using an API token only, or the more traditional (and dangerous) email + API key
	# $5 = Cloudflare API token or API key
	# $6 = Cloudflare email
	
	
	# Example usage:
	# CLOUDFLARE_DDNS "domain.com" "my-ddns.domain.com" "600" "token" "XXXXXXXXXXXXXXX"
	# CLOUDFLARE_DDNS "domain.com" "my-ddns.domain.com" "600" "key+email" "XXXXXXXXXXXXXXX" "name@company.com" 
	
	local ZONE_NAME="${1}"
	local RECORD_NAME="${2}"
	
	if [[ ! -z "${3}" ]]; then
		local RECORD_TTL="${3}"
	else
		local RECORD_TTL=600
	fi
	
	local CLOUDFLARE_AUTH_METHOD="${4}"
	
	if [[ "${CLOUDFLARE_AUTH_METHOD}" = "token" ]]; then
		local CLOUDFLARE_AUTH_TOKEN="${5}"
	elif [[ "${CLOUDFLARE_AUTH_METHOD}" = "key+email" ]]; then
		local CLOUDFLARE_AUTH_KEY="${5}"
		local CLOUDFLARE_AUTH_EMAIL="${6}"
	fi
	
	local PUBLIC_IP="$(dig @1.1.1.1 ch txt whoami.cloudflare +short | tr -d '"')"
	
	local SCRIPT_PATH="$(dirname "$0")"
	
	local IP_FILE="${SCRIPT_PATH}/ddns_${RECORD_NAME}_ip.txt"
	local ID_FILE="${SCRIPT_PATH}/ddns_${RECORD_NAME}_cloudflare_ids.txt"
	local LOG_FILE="${SCRIPT_PATH}/ddns_${RECORD_NAME}_cloudflare.log"
	
	echo "IP_FILE: ${IP_FILE}"
	echo "ID_FILE: ${ID_FILE}"
	echo "LOG_FILE: ${LOG_FILE}"

	# LOGGER
	LOG() {
		if [ "$1" ]; then
			echo -e "[$(date)] - $1" >> $LOG_FILE
		fi
	}
	
	# SCRIPT START
	LOG "Check Initiated"
	
	echo "Public IP: ${PUBLIC_IP}"

	# Check if the IP log file exists
	if [[ -f "${IP_FILE}" ]]; then
		
		# Get the previously-logged IP
		OLD_IP=$(cat ${IP_FILE})
		echo "Logged IP: ${OLD_IP}"
		
		# TODO Implement optional argument for DNS resolver to use
		
		# Short DNS lookup to also check against
		LOOKUP_IP=$(dig @1.1.1.1 ${RECORD_NAME} +short)
		echo "Lookup IP: ${LOOKUP_IP}"
		
		# If the public IP matches the previously-logged IP and the currently-served IP, no update is neccessary
		if [[ "${PUBLIC_IP}" == "${OLD_IP}" ]] && [[ "${PUBLIC_IP}" == "${LOOKUP_IP}" ]]; then
			echo "IP (${PUBLIC_IP}) has not changed."
			exit 0
		fi
	fi
	

	# Check if the zone identifier and record identifier already exist
	if [[ -f "${ID_FILE}" ]] && [[ $(wc -l "${ID_FILE}" | cut -d " " -f 1) == 2 ]]; then
		
		# They already exist; extract them from the file
		ZONE_IDENTIFIER=$(head -1 ${ID_FILE})
		RECORD_IDENTIFIER=$(tail -1 ${ID_FILE})
		
	else
		
		# They don't exist; get them from Cloudflare's API
		
		if [[ "${CLOUDFLARE_AUTH_METHOD}" = "token" ]]; then
			
			ZONE_IDENTIFIER=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=${ZONE_NAME}" -H "Authorization: Bearer ${CLOUDFLARE_AUTH_TOKEN}" -H "Content-Type: application/json" | grep -Po '(?<="id":")[^"]*' | head -1 )
			RECORD_IDENTIFIER=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/${ZONE_IDENTIFIER}/dns_records?name=${RECORD_NAME}" -H "Authorization: Bearer ${CLOUDFLARE_AUTH_TOKEN}" -H "Content-Type: application/json" | grep -Po '(?<="id":")[^"]*')
			echo "${ZONE_IDENTIFIER}" > "${ID_FILE}"
			echo "${RECORD_IDENTIFIER}" >> "${ID_FILE}"

		elif [[ "${CLOUDFLARE_AUTH_METHOD}" = "key+email" ]]; then
			
			ZONE_IDENTIFIER=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones?name=${ZONE_NAME}" -H "X-Auth-Email: ${CLOUDFLARE_AUTH_EMAIL}" -H "X-Auth-Key: ${CLOUDFLARE_AUTH_KEY}" -H "Content-Type: application/json" | grep -Po '(?<="id":")[^"]*' | head -1 )
			RECORD_IDENTIFIER=$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/${ZONE_IDENTIFIER}/dns_records?name=${RECORD_NAME}" -H "X-Auth-Email: ${CLOUDFLARE_AUTH_EMAIL}" -H "X-Auth-Key: ${CLOUDFLARE_AUTH_KEY}" -H "Content-Type: application/json" | grep -Po '(?<="id":")[^"]*')
			echo "${ZONE_IDENTIFIER}" > "${ID_FILE}"
			echo "${RECORD_IDENTIFIER}" >> "${ID_FILE}"
			
		fi

	fi
	
	
	# Perform the update
	if [[ "${CLOUDFLARE_AUTH_METHOD}" = "token" ]]; then
		
		UPDATE=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${ZONE_IDENTIFIER}/dns_records/${RECORD_IDENTIFIER}" -H "Authorization: Bearer ${CLOUDFLARE_AUTH_TOKEN}" -H "Content-Type: application/json" --data "{\"id\":\"${ZONE_IDENTIFIER}\",\"type\":\"A\",\"name\":\"${RECORD_NAME}\",\"content\":\"${PUBLIC_IP}\",\"ttl\":${RECORD_TTL},\"proxied\":false}")
		
	elif [[ "${CLOUDFLARE_AUTH_METHOD}" = "key+email" ]]; then
		
		UPDATE=$(curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${ZONE_IDENTIFIER}/dns_records/${RECORD_IDENTIFIER}" -H "X-Auth-Email: ${CLOUDFLARE_AUTH_EMAIL}" -H "X-Auth-Key: ${CLOUDFLARE_AUTH_KEY}" -H "Content-Type: application/json" --data "{\"id\":\"${ZONE_IDENTIFIER}\",\"type\":\"A\",\"name\":\"${RECORD_NAME}\",\"content\":\"${PUBLIC_IP}\",\"ttl\":${RECORD_TTL},\"proxied\":false}")
		
	fi
	

	# Check if the update was successful
	if [[ "${UPDATE}" == *"\"success\":false"* ]]; then
		MESSAGE="API UPDATE FAILED. DUMPING RESULTS:\n${UPDATE}"
		LOG "${MESSAGE}"
		echo -e "${MESSAGE}"
		exit 1 
	else
		MESSAGE="IP changed to: ${PUBLIC_IP}"
		echo "${PUBLIC_IP}" > "${IP_FILE}"
		LOG "${MESSAGE}"
		echo "${MESSAGE}"
		
		# Pushover alert (if variables exist)
		if [[ ! -z "${PUSHOVER_DEFAULT_USER}" ]] && [[ ! -z "${PUSHOVER_DEFAULT_TOKEN}" ]]; then
			PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "" "${MACHINE_NAME} ${MESSAGE}"	
		fi
		
		# Mailgun alert (if variables exist)
		if [[ ! -z "${MAILGUN_URL}" ]] && [[ ! -z "${MAILGUN_API_KEY}" ]] && [[ ! -z "${MAILGUN_DEFAULT_SENDING_ADDRESS}" ]] && [[ ! -z "${MAILGUN_DEFAULT_RECEIVING_ADDRESS}" ]]; then
			MAILGUN	"${MAILGUN_DEFAULT_RECEIVING_NAME}" "${MAILGUN_DEFAULT_RECEIVING_ADDRESS}" "${MAILGUN_DEFAULT_SENDING_NAME}" "${MAILGUN_DEFAULT_SENDING_ADDRESS}" "" "" "" "" "${MACHINE_NAME} DDNS IP Changed" "${MACHINE_NAME} DDNS IP Changed to ${PUBLIC_IP}" "" ""
		fi
		
	fi

}
   
