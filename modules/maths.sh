#!/usr/bin/env bash

CEIL () {
	
	# Function to round up a number.
	
	# Expected output:
	# An integer
	
	# Input arguments:
	# $1 = Number

	# VARIABLE="$(CEIL 5.2)"
	# echo "$(CEIL 9.458)"

	local NUMBER=${1}

	echo "${NUMBER}" | awk '{print ($0-int($0)>0)?int($0)+1:int($0)}'

}


FLOOR () {
	
	# Function to round down a number.
	
	# Expected output:
	# An integer
	
	# Input arguments:
	# $1 = Number

	# VARIABLE="$(FLOOR 5.98)"
	# echo "$(FLOOR 9.6)"

	local NUMBER=${1}

	echo "${NUMBER}" | awk '{print int($0)}'

}


ROUND () {
	
	# Function to round a number, either up or down.
	
	# Expected output:
	# An integer
	
	# Input arguments:
	# $1 = Number

	# VARIABLE="$(ROUND 5.48)"
	# echo "$(ROUND 9.52)"

	local NUMBER=${1}

	echo "${NUMBER}" | awk '{print ($0-int($0)<0.499)?int($0):int($0)+1}'

}


FORMAT_NUMBER () {
	local NUMBER=$(numfmt --grouping ${1})
	echo "${NUMBER}"
}


DECIMAL () {
	local NUMBER=$(printf "%0.2f\n" ${1})
	echo "${NUMBER}"
}
