#!/usr/bin/env bash

SCRIPT_FULL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # /full/path/containing/the/running/script

MAKE_FILE () {
	
	# Function to make a file and report if sucessful
	
	# Input arguments:
	# $1 = Path/file to create
	
	# Example usage:
	# MAKE_FILE "/home/user/path/to/file_$(TIMESTAMP DATE_NOW)_$(TIMESTAMP TIME_NOW_FULL_FILENAME).txt"
	
	FILE_PATH=${1} # /home/user/folder/file.txt
	
	# Extract directory and filename
	DIRECTORY_PATH=$(dirname "${FILE_PATH}") # /home/user/folder/
	FILENAME=$(basename "${FILE_PATH}") # file.txt
	
	echo "Directory path: ${DIRECTORY_PATH}"
	echo "Filename: ${FILENAME}"
	
	
	# Check if directory exists
	if [[ -d "${DIRECTORY_PATH}" ]]; then
		echo "Directory path (${DIRECTORY_PATH}) exists"
		DIRECTORY_PATH_EXISTENCE=true
	else
		echo "Directory path (${DIRECTORY_PATH}) doesn't exist"
		DIRECTORY_PATH_EXISTENCE=false
	fi
	
	
	# The directory doesn't exist, so we'll try to make it
	if [[ "${DIRECTORY_PATH_EXISTENCE}" = false ]]; then
		
		mkdir -p "${DIRECTORY_PATH}"
		
		# Check if the directory was made
		if [ $? != 0 ] ; then
		    
		    echo "Directory (${DIRECTORY_PATH}) wasn't made; abort"
		    # TODO Pushover
		    exit 1
		    
		else
			
		    echo "Directory (${DIRECTORY_PATH}) was made"
		    DIRECTORY_PATH_EXISTENCE=true
		    
		fi
		
	fi
	
	
	
	
	# The exists (it either already existed or we made it), so we'll try to make the file now
	
	if [[ "${DIRECTORY_PATH_EXISTENCE}" = true ]]; then
		
		echo "Make the file (${FILE_PATH})"
		touch "${FILE_PATH}"
		
		# Check if the file was made
		if [ $? != 0 ] ; then
		    
		    echo "File (${FILE_PATH}) wasn't made; abort"
		    # TODO Pushover
		    exit 1
		    
		else
			
		    echo "File (${FILE_PATH}) was made"
		    
		fi
		
	fi
}




SCRIPT_LOCK_START () {
	
	# Function to check for existence of a given script lock directory.
	# If the script lock doesn't exist, it is made, and the script continues.
	# If the script lock already exists, the user is alerted and the script is aborted.
	
	# Input arguments:
	# $1 = Script lock path (directory)
	
	# Example usage:
	# SCRIPT_LOCK_PATH_START "/home/user/scripts/lock/script-name"
	
	# Check an argument is passed
	if [[ $# -eq 0 ]]; then
		MESSAGE="No argument passed for script lock"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	local SCRIPT_LOCK_PATH=${1}
	
	# Check for existence of script lock
	if [[ -d "${SCRIPT_LOCK_PATH}" ]]; then
		
		# Script lock found
		MESSAGE="Script lock found at ${SCRIPT_LOCK_PATH}. Script is already running or didn't exit cleanly on last run, aborting script."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1

	else
		# Script lock not found
		echo "Script lock not found; continuing script..."
		
		# Make script lock
		echo "Making script lock: ${SCRIPT_LOCK_PATH}"
		mkdir -p "${SCRIPT_LOCK_PATH}"
		
		# If previous command was successful
		if [ $? -eq 0 ]; then
			echo "Script lock created; continuing script..."
		else
			MESSAGE="Failed to create script lock, aborting script."
			echo "${MESSAGE}"
			PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
			exit 1
		fi
			
	fi
	
}


SCRIPT_LOCK_FINISH () {
	
	# Function to delete a script lock directory.
	
	# Input arguments:
	# $1 = Script lock path (directory)
	
	# Example usage:
	# SCRIPT_LOCK_PATH_FINISH "/home/user/scripts/lock/script-name"
	
	# Check an argument is passed
	if [[ $# -eq 0 ]]; then
		MESSAGE="No argument passed for script lock"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	local SCRIPT_LOCK_PATH=${1} # /home/user/scripts/lock/script-name
	
	# Delete script lock
	rmdir "${SCRIPT_LOCK_PATH}"

	# If previous command was successful
	if [[ $? -eq 0 ]]; then
		echo "Script lock deleted"
	else
		MESSAGE="Failed to delete script lock"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
}


CD_CHECK_DIRECTORY_MARKER () {
	
	# Function to cd into the given directory and check for the existence of an immediate child marker directory
	# If the cd is not successful, or the marker directory is not found, the script will be exited
	
	# Input arguments:
	# $1 = Directory (e.g. "/directory/to/change/into")
	# $2 = Directory marker name (e.g. ".marker-5sZsSDmenBDveNf")
	
	# Example usage:
	# CD_CHECK_DIRECTORY_MARKER "/directory/to/change/into" ".marker-5sZsSDmenBDveNf"
	
	# Check an argument is passed
	if [[ $# -eq 0 ]]; then
		local MESSAGE="No arguments passed for directory or marker"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	# Check both arguments aren't empty
	if [[ -z "$1" ]]; then
		local MESSAGE="Empty argument passed for directory"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	if [[ -z "$2" ]]; then
		local MESSAGE="Empty argument passed for marker"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	# Establish variables
	local DIRECTORY=${1}
	local MARKER=${2}
	
	
	# Check for existence of the directory, and that it's a directory
	if [[ -d "${DIRECTORY}" ]]; then
		
		echo "${DIRECTORY} exists and is a directory, continuing..."
		
	else
		
		local MESSAGE="${DIRECTORY} does not exist and/or is not directory, aborting."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
		
	fi
	
	
	# Check for existence of the marker, and that it's a directory
	if [[ -d "${DIRECTORY}/${MARKER}" ]]; then
		
		echo "${DIRECTORY}/${MARKER} marker exists and is a directory, continuing..."
		
	else
		
		local MESSAGE="${DIRECTORY}/${MARKER} marker does not exist and/or is not directory, aborting."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
		
	fi
	
	
	# Change into directory
	cd "${DIRECTORY}"
	
	# Get current directory
	CURRENT_DIRECTORY="$(pwd -P)"
	# See https://unix.stackexchange.com/a/175000 on use of $(pwd) vs ${PWD}
	
	echo ""
	echo "Current directory is ${CURRENT_DIRECTORY}"
	echo ""
	
	
	# Check we are in the directory
	if [[ "${CURRENT_DIRECTORY}" = "${DIRECTORY}" ]];then
	
		echo "We are in the correct directory (${DIRECTORY}), continuing..."

	else
		
		local MESSAGE="We are not in the right directory (${DIRECTORY}), aborting."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
		
	fi
	
	
	# Check that the marker folder is present
	if [[ $(find . -name "${MARKER}" -type d | wc -l) -gt 0 ]]; then
		
		echo "Marker folder '${MARKER}' found; continuing..."
		return
		
	else
		
		local MESSAGE="Marker directorory '${DIRECTORY}/${MARKER}' not found, aborting."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
			
	fi
	
}


SSH_CHECK_DIRECTORY () {
	
	# Function to ssh into a machine and chek for the existence of the given directory
	# If the check is not successful, the script will be exited
	
	# Input arguments:
	# $1 = Directory (e.g. "/directory/to/change/into")
	# $2 = User
	# $3 = IP address/domain name
	# $4 = Port
	
	# Example usage:
	# SSH_CHECK_DIRECTORY "/directory/to/change/into" "username" "000.000.000.000" "999"
	
	# Check an argument is passed
	if [[ $# -eq 0 ]]; then
		local MESSAGE="No arguments passed for directory or marker"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	# Check both arguments aren't empty
	if [[ -z "$1" ]]; then
		local MESSAGE="Empty argument passed for directory"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	if [[ -z "$2" ]]; then
		local MESSAGE="Empty argument passed for user"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	if [[ -z "$3" ]]; then
		local MESSAGE="Empty argument passed for address"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	if [[ -z "$4" ]]; then
		local MESSAGE="Empty argument passed for port"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
	
	# Establish variables
	local DIRECTORY=${1}
	local USER=${2}
	local ADDRESS=${3}
	local PORT=${4}
	
	# Check for existence of the directory, and that it's a directory
	if ssh "${USER}@${ADDRESS}" -p "${PORT}" [[ -d /"${DIRECTORY}" ]]; then
		
		echo "${DIRECTORY} exists and is a directory, continuing..."
		
	else
		
		local MESSAGE="${DIRECTORY} does not exist and/or is not directory, aborting."
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
		
	fi
	
}




MOUNT_LUKS_ENCRYPTED_UUID () {
	
	# Function to mount an encrypted UUID/disk
	
	# Input arguments:
	# $1 = UUID
	# $2 = Mount name (e.g. "backup")
	# $3 = Mount path (e.g. "/backup")
	
	# Example usage:
	# MOUNT_LUKS_ENCRYPTED_UUID "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX" "backup" "/backup"
	
	local UUID=${1}
	local CRYPT_NAME=${2}
	local MOUNT_PATH=${3}

	sudo cryptsetup open --type luks "/dev/disk/by-uuid/${UUID}" "${CRYPT_NAME}" && sudo mount "/dev/mapper/${CRYPT_NAME}" "${MOUNT_PATH}"

}
