#!/usr/bin/env bash

CERTIFICATE_GET_FROM_PFSENSE () {
	
	# Function to get certificate files from a pfSense machine.
	
	# Input arguments:
	# $1 = Domain name of certificate
	# $2 = Directory to download certificate files (with no trailing slash)
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP
	
	# Example usage:
	# CERTIFICATE_GET_FROM_PFSENSE "/path/to/cert-files/domain" "mydomain.net"
	
	local CERT_DOMAIN_NAME=${1}
	local CERT_DOWNLOAD_DIR=${2}
	
	# TODO Check download directory exists
	
	# Get the certificate files
	scp -T "${PFSENSE_CERT_USER}"@"${PFSENSE_IP}:\
	/conf/acme/${CERT_DOMAIN_NAME}.crt \
	/conf/acme/${CERT_DOMAIN_NAME}.key \
	/conf/acme/${CERT_DOMAIN_NAME}.ca" \
	"${CERT_DOWNLOAD_DIR}/"
	
	# TODO Check copied files now exist locally

}


CERTIFICATE_CHECK_EXPIRY_DATE () {
	
	# Function to check the expiry date of a website's certificate
	
	# See https://gist.github.com/cgmartin/49cd0aefe836932cdc96
	
	# Input arguments:
	# $1 = Domain name of certificate
	# $2 = Connect location. If intending to use public-facing/DNS location (e.g. Cloudflare itself), this should be left blank. If wanting to specify the IP address (e.g. a server behind Cloudflare), use it in this argument.
	# $3 = Port number that the server is listening (defaults to 443)
	# $4 = Threshold of days, within which a warning should be made (defaults to 14)
	
	# Example usage:
	
	# Public-facing cert, e.g. Cloudflare itself
	# CERTIFICATE_CHECK_EXPIRY_DATE "mylocalserver.network" "" "" "20"
	
	# Non-public-facing HTTPS cert, e.g. server behind Cloudflare
	# CERTIFICATE_CHECK_EXPIRY_DATE "mylocalserver.network" "81.XXX.XXX.XXX" "" "20"
	
	# Local HTTPS cert
	# CERTIFICATE_CHECK_EXPIRY_DATE "mylocalserver.network" "" "" "20"
	
	# Local cert with non-standard HTTPS port
	# CERTIFICATE_CHECK_EXPIRY_DATE "mylocalserver.network" "" "567" "20"
	
	# Local cert with IP address and non-standard HTTPS port
	# CERTIFICATE_CHECK_EXPIRY_DATE "mylocalserver.network" "192.168.X.X" "567" "20"
	
	local CERT_DOMAIN_NAME=${1}
	local CONNECT_LOCATION=${2}
	local PORT_NUMBER=${3}
	local DAYS_THRESHOLD=${4}
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=443
	fi
	
	# Establish connect location
	if [[ ! -z "${CONNECT_LOCATION}" ]]; then
		local CONNECT_LOCATION="${CONNECT_LOCATION}"
	else
		local CONNECT_LOCATION="${CERT_DOMAIN_NAME}"
	fi

	#echo "Checking if ${CERT_DOMAIN_NAME} expires in less than ${DAYS_THRESHOLD} days";

	local CERT_EXPIRATION_DATE=$(date -d "$(: | openssl s_client -connect ${CONNECT_LOCATION}:${PORT_NUMBER} -servername ${CERT_DOMAIN_NAME} 2>/dev/null \
	| openssl x509 -text \
	| grep 'Not After' \
	| awk '{print $4,$5,$7}')" '+%s')

	local CERT_EXPIRATION_DATE_ISO="$(date -d @${CERT_EXPIRATION_DATE} '+%Y-%m-%d')"

	# local EXPIRY_THRESHOLD_DATE=$(($(date +%s) + (86400*${DAYS_THRESHOLD})))

	# if [[ "${EXPIRY_THRESHOLD_DATE}" -gt "${CERT_EXPIRATION_DATE}" ]]; then
 #    	echo "WARNING! The certificate for ${CERT_DOMAIN_NAME} expires in less than ${DAYS_THRESHOLD} days, on $(date -d @${CERT_EXPIRATION_DATE} '+%Y-%m-%d')..."
 #    	return 1
	# else
 #    	echo "All clear. Certificate expires on $(date -d @${CERT_EXPIRATION_DATE} '+%Y-%m-%d')"
 #    	return 0
	# fi
	
	echo "${CERT_EXPIRATION_DATE_ISO}"

}


UNIFI_VIDEO_CERT_UPDATE_REMOTE () {
	
	# Function to install/replace a new HTTPS certifiate for UniFi Video on another machine via SSH.
	
	# Input arguments:
	# $1 = IP address of remote machine
	# $2 = Port number for remote machine SSH (defaults to 22)
	# $3 = Domain name used for certificate
	# $4 = Path to directory used to store the certificates on the local machine running the script. Don't include a tailing slash.
	
	# Example usage:
	# UNIFI_VIDEO_CERT_UPDATE_REMOTE "192.168.1.x" "22" "unifi-video.domain.com" "/path/to/certificates/unifi-video"
	
	# TODO:
	# Utilise the CERTIFICATE_GET_FROM_PFSENSE function, instead of hardcoding the commands in this function
	# Verify success at each step before proceeding


	local DEVICE_IP="${1}"
	local PORT_NUMBER=${2}
	local CERT_DOMAIN="${3}"
	local STORAGE_DIR="${4}"
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	echo "DEVICE_IP:   ${DEVICE_IP}"
	echo "PORT_NUMBER: ${PORT_NUMBER}"
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"


	local PFSENSE_CA="${CERT_DOMAIN}.ca"
	local PFSENSE_CERT="${CERT_DOMAIN}.crt"
	local PFSENSE_KEY="${CERT_DOMAIN}.key"


	local STORED_CERT_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem"
	local STORED_KEY_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.key.pem"
	local STORED_COMBINED_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem"
	
	local GENERATED_CERT_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.cert.der"
	local GENERATED_KEY_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.key.der"


	# Copy certificates from router
	scp "${ROUTER_PFSENSE_USER}"@"${ROUTER_PFSENSE_CERT_PATH}${PFSENSE_CERT}" "${STORED_CERT_PATH}"
	scp "${ROUTER_PFSENSE_USER}"@"${ROUTER_PFSENSE_CERT_PATH}${PFSENSE_KEY}" "${STORED_KEY_PATH}"
	
	
	# Generate the certificate files in the format needed
	openssl x509 -outform der -in "${STORED_CERT_PATH}" -out "${GENERATED_CERT_PATH}"
	openssl pkcs8 -topk8 -nocrypt -in "${STORED_KEY_PATH}" -outform DER -out "${GENERATED_KEY_PATH}"


	# Stop UniFi Video
	echo "Stopping UniFi Video..."
	ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" "sudo service unifi-video stop"
	
	
	# Backup existing files
	ssh -t root@"${DEVICE_IP}" -p "${PORT_NUMBER}" "\
		sudo /bin/cp /var/lib/unifi-video/data/certificates/ufv-server.key.der /var/lib/unifi-video/data/certificates/ufv-server.key.der.backup.$(date +%F_%R) && \
		sudo /bin/cp /var/lib/unifi-video/data/certificates/ufv-server.cert.der /var/lib/unifi-video/data/certificates/ufv-server.cert.der.backup.$(date +%F_%R) && \
		sudo /bin/cp /var/lib/unifi-video/ufv-truststore /var/lib/unifi-video/ufv-truststore.backup.$(date +%F_%R) && \
		sudo /bin/cp /var/lib/unifi-video/keystore /var/lib/unifi-video/keystore.backup.$(date +%F_%R) && \
		sudo /bin/cp /var/lib/unifi-video/system.properties /var/lib/unifi-video/system.properties.backup.$(date +%F_%R)"
		
	
	# Copy the generated certificates to the server
	scp -P "${PORT_NUMBER}" "${GENERATED_CERT_PATH}" root@"${DEVICE_IP}":"/usr/lib/unifi-video/data/certificates/ufv-server.cert.der"
	scp -P "${PORT_NUMBER}" "${GENERATED_KEY_PATH}" root@"${DEVICE_IP}":"/usr/lib/unifi-video/data/certificates/ufv-server.key.der"
	
	
	# Set certificate permissions and remove old files (these must be removed for the new certificartes to be properly imported on startup)
	ssh -t root@"${DEVICE_IP}" -p "${PORT_NUMBER}" "\
		sudo chown -R unifi-video:unifi-video /usr/lib/unifi-video/data/certificates && \
		sudo rm /var/lib/unifi-video/ufv-truststore && \
		sudo rm /var/lib/unifi-video/keystore && \
		sudo rm /var/lib/unifi-video/conf/evostream/server.*"
	
	# Start UniFi Video
	echo "Starting UniFi Video..."
	ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" "sudo service unifi-video start"
	
}



UNIFI_VIDEO_CERT_UPDATE_LOCAL () {

	# Function to install/replace a new HTTPS certifiate for UniFi Video on the local machine.
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. Don't include a tailing slash.
	
	# Example usage:
	# UNIFI_VIDEO_CERT_UPDATE_LOCAL "unifi-video.domain.com" "/path/to/certificates/unifi-video"
	
	# TODO:
	# Verify success at each step before proceeding

	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	REQUIRE_ROOT

	local UNIFIVIDEO_USER="unifi-video"
	local UNIFIVIDEO_CERT_DEST_DIR="/usr/lib/unifi-video/data/certificates"
	local UNIFIVIDEO_STORAGE_DIR="/var/lib/unifi-video"

	local BACKUP_FILE_DATE_FORMAT=$(date +%F_%R)

	local UNIFIVIDEO_INPUT_CERT_FILENAME="${CERT_DOMAIN}.crt"
	local UNIFIVIDEO_INPUT_KEY_FILENAME="${CERT_DOMAIN}.key"

	local UNIFIVIDEO_INPUT_CERT_FILEPATH="${STORAGE_DIR}/${UNIFIVIDEO_INPUT_CERT_FILENAME}"
	local UNIFIVIDEO_INPUT_KEY_FILEPATH="${STORAGE_DIR}/${UNIFIVIDEO_INPUT_KEY_FILENAME}"

	local UNIFIVIDEO_RENAMED_CERT_FILENAME="${CERT_DOMAIN}.crt.pem"
	local UNIFIVIDEO_RENAMED_KEY_FILENAME="${CERT_DOMAIN}.key.pem"

	local UNIFIVIDEO_RENAMED_CERT_FILEPATH="${STORAGE_DIR}/${UNIFIVIDEO_RENAMED_CERT_FILENAME}"
	local UNIFIVIDEO_RENAMED_KEY_FILEPATH="${STORAGE_DIR}/${UNIFIVIDEO_RENAMED_KEY_FILENAME}"

	local UNIFIVIDEO_OUTPUT_CERT_FILENAME="ufv-server.cert.der"
	local UNIFIVIDEO_OUTPUT_KEY_FILENAME="ufv-server.key.der"

	local UNIFIVIDEO_OUTPUT_CERT_FILEPATH="${UNIFIVIDEO_CERT_DEST_DIR}/${UNIFIVIDEO_OUTPUT_CERT_FILENAME}"
	local UNIFIVIDEO_OUTPUT_KEY_FILEPATH="${UNIFIVIDEO_CERT_DEST_DIR}/${UNIFIVIDEO_OUTPUT_KEY_FILENAME}"


	local PFSENSE_UNIFIVIDEO_CERT="${CERT_DOMAIN}.crt"
	local PFSENSE_UNIFIVIDEO_KEY="${CERT_DOMAIN}.key"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	echo "Stop the UniFi Video service"
	# Stop the UniFi Video service
	sudo service unifi-video stop

	# Make renamed files
	echo "Make renamed files"
	/bin/cp "${STORAGE_DIR}/${UNIFIVIDEO_INPUT_CERT_FILENAME}" "${STORAGE_DIR}/${UNIFIVIDEO_RENAMED_CERT_FILENAME}"
	/bin/cp "${STORAGE_DIR}/${UNIFIVIDEO_INPUT_KEY_FILENAME}" "${STORAGE_DIR}/${UNIFIVIDEO_RENAMED_KEY_FILENAME}"

	# Backup previous files
	echo "Backup previous files"
	cp "${UNIFIVIDEO_STORAGE_DIR}/data/certificates/${UNIFIVIDEO_OUTPUT_CERT_FILENAME}" "${UNIFIVIDEO_STORAGE_DIR}/data/certificates/${UNIFIVIDEO_OUTPUT_CERT_FILENAME}.backup.${BACKUP_FILE_DATE_FORMAT}"
	cp "${UNIFIVIDEO_STORAGE_DIR}/data/certificates/${UNIFIVIDEO_OUTPUT_KEY_FILENAME}" "${UNIFIVIDEO_STORAGE_DIR}/data/certificates/${UNIFIVIDEO_OUTPUT_KEY_FILENAME}.backup.${BACKUP_FILE_DATE_FORMAT}"
	cp "${UNIFIVIDEO_STORAGE_DIR}/ufv-truststore" "${UNIFIVIDEO_STORAGE_DIR}/ufv-truststore.backup.${BACKUP_FILE_DATE_FORMAT}"
	cp "${UNIFIVIDEO_STORAGE_DIR}/keystore" "${UNIFIVIDEO_STORAGE_DIR}/keystore.backup.${BACKUP_FILE_DATE_FORMAT}"
	cp "${UNIFIVIDEO_STORAGE_DIR}/system.properties" "${UNIFIVIDEO_STORAGE_DIR}/system.properties.backup.${BACKUP_FILE_DATE_FORMAT}"

	# Delete currently-in use cert files
	echo "Delete currently-in use cert files"
	rm "${UNIFIVIDEO_STORAGE_DIR}/ufv-truststore"
	rm "${UNIFIVIDEO_STORAGE_DIR}/keystore"

	# Never exists anyway
	####rm /var/lib/unifi-video/conf/evostream/server.*

	# Convert to cert formats
	echo "Convert to cert formats"
	openssl x509 -in "${UNIFIVIDEO_RENAMED_CERT_FILEPATH}" -outform der -out "${UNIFIVIDEO_OUTPUT_CERT_FILEPATH}"
	openssl pkcs8 -topk8 -nocrypt -in "${UNIFIVIDEO_RENAMED_KEY_FILEPATH}" -outform DER -out "${UNIFIVIDEO_OUTPUT_KEY_FILEPATH}"


	# Set permissions on destination directory
	echo "Set permissions on destination directory"
	chown -R "${UNIFIVIDEO_USER}":"${UNIFIVIDEO_USER}" "${UNIFIVIDEO_CERT_DEST_DIR}"


	# Start the UniFi Video service
	echo "Start the UniFi Video service"
	sudo service unifi-video start

}



SYNCTHING_CERT_UPDATE_LOCAL () {
	
	# Function to install/replace a new HTTPS certifiate for Syncthing on the local machine.
	
	# Required variables:
	# SYNCTHING_API_KEY
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. Don't include a tailing slash.
	# $3 = Path to directory Syncthing uses for its certificate. Don't include a tailing slash.
	
	# Example usage:
	# SYNCTHING_CERT_UPDATE_LOCAL "syncthing.domain.com" "/path/to/certificates/syncthing" "/home/username/.config/syncthing"
	
	# TODO:
	# Utilise the CERTIFICATE_GET_FROM_PFSENSE function, instead of hardcoding the commands in this function
	# Hopefully remove the entire CERT_DIR argument, since we can probably use the default path and current username for the system
	# Check for (and remove if present) trailing slashes in the directory arguments
	# Verify success at each step before proceeding
	
	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	local CERT_DIR="${3}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"
	echo "CERT_DIR:    ${CERT_DIR}"

	local SYNCTHING_CERT_FILENAME="https-cert.pem"
	local SYNCTHING_KEY_FILENAME="https-key.pem"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"


	# Make renamed files
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.crt" "${STORAGE_DIR}/${SYNCTHING_CERT_FILENAME}"
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.key" "${STORAGE_DIR}/${SYNCTHING_KEY_FILENAME}"

	# Copy files to Syncthing directory
	# "/bin/cp" used, since "cp" alone is aliased to "cp -i" by default, resulting in prompts to overwrite
	/bin/cp "${STORAGE_DIR}/${SYNCTHING_CERT_FILENAME}" "${CERT_DIR}/${SYNCTHING_CERT_FILENAME}"
	/bin/cp "${STORAGE_DIR}/${SYNCTHING_KEY_FILENAME}" "${CERT_DIR}/${SYNCTHING_KEY_FILENAME}"

	# Restart Syncthing
	curl -X POST -k -H "X-API-Key: ${SYNCTHING_API_KEY}" "https://localhost:8384/rest/system/restart"

}


UNIFI_CLOUD_KEY_CERT_UPDATE_REMOTE () {
	
	# Function to install/replace a new HTTPS certifiate for UniFi Controller on another machine via SSH.
	
	# Input arguments:
	# $1 = IP address of UniFi Cloud Key
	# $2 = Port number for remote machine SSH (defaults to 22)
	# $3 = Domain name used for certificate
	# $4 = Path to directory used to store the certificates on the local machine running the script. Don't include a tailing slash.
	
	# Example usage:
	# UNIFI_CLOUD_KEY_CERT_UPDATE_REMOTE "192.168.1.x" "22" "unifi.domain.com" "/path/to/certificates/unifi-controller"
	
	# TODO:
	# Utilise the CERTIFICATE_GET_FROM_PFSENSE function, instead of hardcoding the commands in this function
	# Possibly adapt this function to work for all remote UniFi Controllers, not just Cloud Keys?
	# Verify success at each step before proceeding

	# 2016-12 https://scotthelme.co.uk/setting-up-https-on-the-unifi-cloudkey/
	# 2017-12 https://community.ui.com/questions/How-To-Lets-Encrypt-with-Cloud-Key-and-DNS-Challenge/5dc4f896-3e60-42a2-82b8-2cf7fe2279fc
	# 2018-01 https://community.ui.com/stories/Adding-Lets-Encrypt-certificate-to-UniFi-Cloud-Key-without-exposing-UniFi-to-the-internet/709219e3-32ee-41a8-a0a3-746e8a67e27e
	# 2018-09 https://blog.barclayhowe.com/letsencrypt-ssl-certificate-pfsense-internal-linux-server/
	# 2019-04 https://community.ui.com/questions/how-to-login-to-cloud-key-with-SSH-public-key-authentication/70ef2982-6f6d-47fc-8de1-c493c64f7747#answer/1ebdb30a-5ec3-45b0-99b7-a68a9f2bc9df

	local DEVICE_IP="${1}"
	local PORT_NUMBER=${2}
	local CERT_DOMAIN="${3}"
	local STORAGE_DIR="${4}"
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	echo "DEVICE_IP:   ${DEVICE_IP}"
	echo "PORT_NUMBER: ${PORT_NUMBER}"
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	#local PFSENSE_UNIFI_CERT="${CERT_DOMAIN}.crt"
	#local PFSENSE_UNIFI_KEY="${CERT_DOMAIN}.key"

	local UNIFI_INPUT_CERT_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.crt"
	local UNIFI_INPUT_KEY_PATH="${STORAGE_DIR}/${CERT_DOMAIN}.key"

	local UNIFI_OUTPUT_P12_PATH="${STORAGE_DIR}/unifi.p12"

	local UNIFI_CERT_USERNAME="unifi"
	local UNIFI_CERT_PASSWORD="aircontrolenterprise"

	local CLOUDKEY_CERT_STORAGE_PATH="/usr/lib/unifi/certificates/unifi.p12"


	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Package certificate into a p12
	openssl pkcs12 -export -in "${UNIFI_INPUT_CERT_PATH}" -inkey "${UNIFI_INPUT_KEY_PATH}" -out "${UNIFI_OUTPUT_P12_PATH}" -name "${UNIFI_CERT_USERNAME}" -caname root -passout pass:"${UNIFI_CERT_PASSWORD}"

	# Copy the p12 to the Cloudkey
	scp -P "${PORT_NUMBER}" "${UNIFI_OUTPUT_P12_PATH}" root@"${DEVICE_IP}":"${CLOUDKEY_CERT_STORAGE_PATH}"

	# Backup the current certificates and keystore
	ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" 'DATETIME="$(date +"%F")_$(date +"%H-%M-%S")" && mkdir /usr/lib/unifi/certificates/backup_"${DATETIME}" && cp -r /etc/ssl/private/. /usr/lib/unifi/certificates/backup_"${DATETIME}"'


	# Get the certificate into the keystore
	ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" 'keytool -importkeystore -deststorepass aircontrolenterprise -destkeypass aircontrolenterprise -destkeystore /etc/ssl/private/unifi.keystore.jks -srckeystore /usr/lib/unifi/certificates/unifi.p12 -srcstoretype PKCS12 -alias unifi -srcstorepass aircontrolenterprise'


	# Restart Unifi - this can take a couple of minutes
	echo "Restarting UniFi - this can take a couple of minutes"
	ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" 'service unifi restart'

}



UNIFI_CONTROLLER_CERT_UPDATE_LOCAL () {
	
	# https://blog.barclayhowe.com/letsencrypt-ssl-certificate-pfsense-internal-linux-server/
	
	# Function to install/replace a new HTTPS certifiate for UniFi Controller on another machine via SSH.
	
	# Input arguments:
	# $2 = Domain name used for certificate
	# $3 = Path to directory used to store the certificates on the local machine running the script. Don't include a tailing slash.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP
	
	# Example usage:
	# UNIFI_CONTROLLER_CERT_UPDATE_LOCAL "unifi.domain.com" "/path/to/certificates/unifi-controller"
	
	# TODO:
	# Utilise the CERTIFICATE_GET_FROM_PFSENSE function, instead of hardcoding the commands in this function
	# Verify success at each step before proceeding

	# 2016-12 https://scotthelme.co.uk/setting-up-https-on-the-unifi-cloudkey/
	# 2017-12 https://community.ui.com/questions/How-To-Lets-Encrypt-with-Cloud-Key-and-DNS-Challenge/5dc4f896-3e60-42a2-82b8-2cf7fe2279fc
	# 2018-01 https://community.ui.com/stories/Adding-Lets-Encrypt-certificate-to-UniFi-Cloud-Key-without-exposing-UniFi-to-the-internet/709219e3-32ee-41a8-a0a3-746e8a67e27e
	# 2018-09 https://blog.barclayhowe.com/letsencrypt-ssl-certificate-pfsense-internal-linux-server/
	# 2019-04 https://community.ui.com/questions/how-to-login-to-cloud-key-with-SSH-public-key-authentication/70ef2982-6f6d-47fc-8de1-c493c64f7747#answer/1ebdb30a-5ec3-45b0-99b7-a68a9f2bc9df
	
	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	REQUIRE_ROOT

	local UNIFI_CERT_DEFAULT_USER="unifi"
	local UNIFI_CERT_DEFAULT_PASSWORD="unifi"

	# local PFSENSE_UNIFI_CERT="${CERT_DOMAIN}.crt"
	# local PFSENSE_UNIFI_KEY="${CERT_DOMAIN}.key"

	local UNIFI_INPUT_CERT="${STORAGE_DIR}/${CERT_DOMAIN}.crt"
	local UNIFI_INPUT_KEY="${STORAGE_DIR}/${CERT_DOMAIN}.key"

	local UNIFI_RENAMED_CERT="${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem"
	local UNIFI_RENAMED_KEY="${STORAGE_DIR}/${CERT_DOMAIN}.key.pem"

	local UNIFI_PROCESSED_P12="${STORAGE_DIR}/${CERT_DOMAIN}.p12"

	local UNIFI_KEYSTORE="/var/lib/unifi/keystore"





	local UNIFI_BACKUP_DIR="${STORAGE_DIR}/old/$(date +%F_%R)"

	local UNIFI_BACKUP_INPUT_CERT="${UNIFI_BACKUP_DIR}/${CERT_DOMAIN}.crt"
	local UNIFI_BACKUP_INPUT_KEY="${UNIFI_BACKUP_DIR}/${CERT_DOMAIN}.key"

	local UNIFI_BACKUP_RENAMED_CERT="${UNIFI_BACKUP_DIR}/${CERT_DOMAIN}.crt.pem"
	local UNIFI_BACKUP_RENAMED_KEY="${UNIFI_BACKUP_DIR}/${CERT_DOMAIN}.key.pem"

	local UNIFI_BACKUP_PROCESSED_P12="${UNIFI_BACKUP_DIR}/${CERT_DOMAIN}.p12"

	# Make backup directory
	sudo -u "$USER" mkdir "${UNIFI_BACKUP_DIR}"


	# Remove/backup old files
	sudo -u "$USER" /bin/cp "${UNIFI_KEYSTORE}" "${UNIFI_BACKUP_DIR}/"
	sudo -u "$USER" /bin/cp "${UNIFI_INPUT_CERT}" "${UNIFI_BACKUP_INPUT_CERT}"
	sudo -u "$USER" /bin/cp "${UNIFI_INPUT_CERT}" "${UNIFI_BACKUP_INPUT_KEY}"
	sudo -u "$USER" /bin/cp "${UNIFI_RENAMED_CERT}" "${UNIFI_BACKUP_RENAMED_CERT}"
	sudo -u "$USER" /bin/cp "${UNIFI_RENAMED_KEY}" "${UNIFI_BACKUP_RENAMED_KEY}"
	sudo -u "$USER" /bin/cp "${UNIFI_PROCESSED_P12}" "${UNIFI_BACKUP_PROCESSED_P12}"

	# Copy certificates from router
	# TODO: The hardcoded commands here were using sudo -u "$USER" - there might be a need to somehow replicate this behaviour instead of running the function as root
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Make renamed files
	sudo -u "$USER" /bin/cp "${UNIFI_INPUT_CERT}" "${UNIFI_RENAMED_CERT}"
	sudo -u "$USER" /bin/cp "${UNIFI_INPUT_KEY}" "${UNIFI_RENAMED_KEY}"

	# Convert cert to PKCS12 format
	sudo -u "$USER" openssl pkcs12 -export -inkey "${UNIFI_RENAMED_KEY}" -in "${UNIFI_RENAMED_CERT}" -out "${UNIFI_PROCESSED_P12}" -name "${UNIFI_CERT_DEFAULT_USER}" -password pass:"${UNIFI_CERT_DEFAULT_PASSWORD}"

	# Import certificate
	keytool -importkeystore -deststorepass aircontrolenterprise -destkeypass aircontrolenterprise -destkeystore "${UNIFI_KEYSTORE}" -srckeystore "${UNIFI_PROCESSED_P12}" -srcstoretype PKCS12 -srcstorepass unifi -alias unifi -noprompt

	# Restart the UniFi controller
	service unifi restart
	
}



COUCHPOTATO_CERT_UPDATE_LOCAL () {

	# Function to install/replace a new HTTPS certifiate for CouchPotato on the local machine.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. CouchPotato settings should also be updated to use this directory. Don't include a tailing slash.
	
	# Example usage:
	# COUCHPOTATO_CERT_UPDATE_LOCAL "couchpotato.domain.com" "/path/to/certificates/couchpotato"
	
	# TODO:
	# Check for (and remove if present) trailing slash in the directory argument
	# Verify success at each step before proceeding
	
	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"
	
}



NZBGET_CERT_UPDATE_LOCAL () {

	# Function to install/replace a new HTTPS certifiate for CouchPotato on the local machine.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP
	# NZBGET_LOCATION
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. CouchPotato settings should also be updated to use this directory. Don't include a tailing slash.
	
	# Example usage:
	# NZBGET_CERT_UPDATE_LOCAL "nzbget.domain.com" "/path/to/certificates/nzbget"
	
	# TODO:
	# Check for (and remove if present) trailing slash in the directory argument
	# Verify success at each step before proceeding

	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Make renamed files
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.crt" "${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem"
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.key" "${STORAGE_DIR}/${CERT_DOMAIN}.key.pem"

	# Stop NZBGet
	echo "Stopping NZBGet..."
	"${NZBGET_LOCATION}" -Q

	sleep 5s

	# Start NZBGet
	echo "Starting NZBGet..."
	"${NZBGET_LOCATION}" -D

}



PIHOLE_CERT_UPDATE_LOCAL () {

	# Function to install/replace a new HTTPS certifiate for CouchPotato on the local machine.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. CouchPotato settings should also be updated to use this directory. Don't include a tailing slash.
	
	# Example usage:
	# PIHOLE_CERT_UPDATE_LOCAL "pihole.domain.com" "/path/to/certificates/pihole"
	
	# TODO:
	# Check for existence of CA file. If not present (e.g. it's a first run), grab it and process it
	# Check for (and remove if present) trailing slash in the directory argument
	# Verify success at each step before proceeding
	
	REQUIRE_ROOT

	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	#PIHOLE_INPUT_CA="${STORAGE_DIR}/lets-encrypt.ca.crt"
	#PIHOLE_RENAMED_CA="${STORAGE_DIR}/${CERT_DOMAIN}.ca.pem"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Make renamed files
	sudo -u "${USER}" /bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.crt" "${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem"
	sudo -u "${USER}" /bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.key" "${STORAGE_DIR}/${CERT_DOMAIN}.key.pem"

	# Remove old combined file
	sudo -u "${USER}" mv "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem" "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem.old.$(date +%F_%R)"

	# Make combined cert file
	sudo -u "${USER}" cat "${STORAGE_DIR}/${CERT_DOMAIN}.key.pem" "${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem" | tee "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem"

	# Change ownership of cert directory
	sudo chown www-data -R "${STORAGE_DIR}"

	# Restart Pi-hole web server
	service lighttpd restart

}



SONARR_CERT_UPDATE_LOCAL () {

	# Function to install/replace a new HTTPS certifiate for Sonarr on the local machine.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP

	# Prerequisites:
	# Autostart and service setup as https://github.com/Sonarr/Sonarr/wiki/Autostart-on-Linux
	
	# Input arguments:
	# $1 = Domain name used for certificate
	# $2 = Path to directory used to store the certificates on the local machine running the script. CouchPotato settings should also be updated to use this directory. Don't include a tailing slash.
	
	# Example usage:
	# SONARR_CERT_UPDATE_LOCAL "sonarr.domain.com" "/path/to/certificates/sonarr"
	
	# TODO:
	# Check for existence of CA file. If not present (e.g. it's a first run), grab it and process it
	# Check for (and remove if present) trailing slash in the directory argument
	# Verify success at each step before proceeding
	

	local CERT_DOMAIN="${1}"
	local STORAGE_DIR="${2}"
	
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"

	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Make renamed files
	# "/bin/cp" used, since "cp" alone is aliased to "cp -i" by default, resulting in prompts to overwrite
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.crt" "${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem"
	/bin/cp "${STORAGE_DIR}/${CERT_DOMAIN}.key" "${STORAGE_DIR}/${CERT_DOMAIN}.key.pem"

	# Convert cert to .cert
	openssl x509 -inform PEM -in "${STORAGE_DIR}/${CERT_DOMAIN}.crt.pem" -outform DER -out "${STORAGE_DIR}/${CERT_DOMAIN}.crt.cert"

	# Convert key to pvk
	openssl rsa -in "${STORAGE_DIR}/${CERT_DOMAIN}.key.pem" -outform PVK -pvk-none -out "${STORAGE_DIR}/${CERT_DOMAIN}.key.pvk"

	# Delete old cert
	httpcfg -del -port 9898

	# Install new cert to Sonarr
	httpcfg -add -port 9898 -pvk "${STORAGE_DIR}/${CERT_DOMAIN}.key.pvk" -cert "${STORAGE_DIR}/${CERT_DOMAIN}.crt.cert"

	# Stop Sonarr
	sudo systemctl stop sonarr.service

	# PID=$(ps -eaf | grep mono | grep -v grep | awk '{print $2}')
	# if [[ "" !=  "${PID}" ]]; then
	# 	echo "Killing PID ${PID}"
	# 	kill -9 "${PID}"
	# fi

	sleep 2s

	sudo systemctl start sonarr.service

}




PIHOLE_CERT_UPDATE_REMOTE () {

	# 2018-04 https://scotthelme.co.uk/securing-dns-across-all-of-my-devices-with-pihole-dns-over-https-1-1-1-1/#bonusround

	# Function to install/replace a new HTTPS certifiate for Sonarr on the local machine.
	
	# Required variables:
	# PFSENSE_CERT_USER
	# PFSENSE_IP

	# Prerequisites:
	# Before using the automated certificate update, we must first edit:
	# sudo nano /etc/lighttpd/external.conf

	# In this file, we must put (adjust domain and certificate paths as necessary:
	# $HTTP["host"] == "the-domain-being-used.com" {
	# 	# Ensure the Pi-hole Block Page knows that this is not a blocked domain
	# 	setenv.add-environment = ("fqdn" => "true")
	# 
	# 	# Enable the SSL engine with a LE cert, only for this specific host
	# 	$SERVER["socket"] == ":443" {
	# 		ssl.engine = "enable"
	# 		ssl.pemfile = "/home/pi/certificates/the-domain-being-used.com.combined.pem"
	# 		ssl.ca-file =  "/home/pi/certificates/the-domain-being-used.com.intermediate.pem"
	# 		ssl.honor-cipher-order = "enable"
	# 		ssl.cipher-list = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH"
	# 		ssl.use-compression = "disable"
	# 		ssl.use-sslv2 = "disable"
	# 		ssl.use-sslv3 = "disable"
	# 	}
	# 
	# 	# Redirect HTTP to HTTPS
	# 	$HTTP["scheme"] == "http" {
	# 		$HTTP["host"] =~ ".*" {
	# 			url.redirect = (".*" => "https://%0$0")
	# 		}
	# 	}
	# }
	
	# Input arguments:
	# $1 = IP address of remote machine
	# $2 = Port number for remote machine SSH (defaults to 22)
	# $3 = Domain name used for certificate
	# $4 = Path to directory used to store the certificates on the local machine running the script. CouchPotato settings should also be updated to use this directory. Don't include a tailing slash.
	# $5 = The user on the remote machine, used for SSH.
	# $6 = The directory used to store certificate files on the remote machine.
	
	# Example usage:
	# PIHOLE_CERT_UPDATE_REMOTE "192.168.1.x" "22" "pihole.domain.com" "/path/to/certificates/pihole" "pi" "/home/pi/certificates"
	
	# TODO:
	# Check for (and remove if present) trailing slash in the directory argument
	# Verify success at each step before proceeding


	local DEVICE_IP="${1}"
	local PORT_NUMBER=${2}
	local CERT_DOMAIN="${3}"
	local STORAGE_DIR="${4}"
	local REMOTE_USER="${5}"
	local REMOTE_DIR="${6}"
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	echo "DEVICE_IP:   ${DEVICE_IP}"
	echo "PORT_NUMBER: ${PORT_NUMBER}"
	echo "CERT_DOMAIN: ${CERT_DOMAIN}"
	echo "STORAGE_DIR: ${STORAGE_DIR}"
	echo "REMOTE_USER: ${REMOTE_USER}"
	echo "REMOTE_DIR:  ${REMOTE_DIR}"
	
	# Copy certificates from router
	CERTIFICATE_GET_FROM_PFSENSE "${CERT_DOMAIN}" "${STORAGE_DIR}"

	# Backup old combined file
	mv "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem" "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem.old.$(date +%F_%H-%M-%S)"
	
	# Make combined cert file
	cat "${STORAGE_DIR}/${CERT_DOMAIN}.key" "${STORAGE_DIR}/${CERT_DOMAIN}.crt" | tee "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem"
	
	# Make the required directories on the Pi-hole
	ssh "${REMOTE_USER}"@"${DEVICE_IP}" -p "${PORT_NUMBER}" "mkdir -p ${REMOTE_DIR}"
	
	# Copy the certificate files to the Pi-hole
	scp -P "${PORT_NUMBER}" "${STORAGE_DIR}/${CERT_DOMAIN}.ca" "${REMOTE_USER}"@"${DEVICE_IP}":"${REMOTE_DIR}/${CERT_DOMAIN}.intermediate.pem"
	scp -P "${PORT_NUMBER}" "${STORAGE_DIR}/${CERT_DOMAIN}.combined.pem" "${REMOTE_USER}"@"${DEVICE_IP}":"${REMOTE_DIR}/${CERT_DOMAIN}.combined.pem"
	
	# Restart the lighttpd service
	ssh "${REMOTE_USER}"@"${DEVICE_IP}" -p "${PORT_NUMBER}" "sudo service lighttpd restart"

}
