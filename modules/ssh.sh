#!/usr/bin/env bash

SSH_PUBLIC_KEY_PRESENCE () {
	
	# Function to detect if a particular SSH key is already added.
	
	# Input arguments:
	# $1 = SSH public key (e.g. "ssh-ed25519 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX user@machine")
	
	# Example usage:
	# SSH_PUBLIC_KEY_PRESENCE "ssh-ed25519 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX user@machine"
	
	local SSH_PUBLIC_KEY=${1}
	
	SSH_ADD_LIST=$(ssh-add -L)
	echo "SSH agent repsonse: ${SSH_ADD_LIST}"
	
	if [[ $(echo "${SSH_ADD_LIST}" | grep "${SSH_PUBLIC_KEY}") ]]; then
		echo "SSH key found"
		return
	else
		MESSAGE="SSH key not found"
		echo "${MESSAGE}"
		PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} FAILED" "${MESSAGE}"
		exit 1
	fi
}
