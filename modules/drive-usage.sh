#!/usr/bin/env bash

DIRECTORY_SIZE () {
	
	# Function to get the drive usage of a directory.
	
	# Expected output:
	# A human-readable number in either KB, MB, GB or TB.
	
	# Input arguments:
	# $1 = Directory
	# $2 = Format to output the number [Blank = Natural, KB, MB, GB, TB]
	
	# Example usage:
	# DIRECTORY_SIZE "/path/to/directory"
	# DIRECTORY_SIZE "/path/to/directory" "KB"
	# DIRECTORY_SIZE "/path/to/directory" "MB"
	# DIRECTORY_SIZE "/path/to/directory" "GB"
	# DIRECTORY_SIZE "/path/to/directory" "TB"
	# VARIABLE="$(DIRECTORY_SIZE "/path/to/directory")"
	
	# Resources:
	# https://askubuntu.com/a/454969
	
	DU_SIZE=$(du -s "${1}" | cut -f1)
	
	if [[ -z "${2}" ]]; then
		
		if [[ "${DU_SIZE}" -lt "1024" ]]; then
			
			# Use KB
			NUMBER="$(echo "scale=2; ${DU_SIZE}" | bc) KB"
			
		elif [[ "${DU_SIZE}" -lt "1048576" ]]; then
			
			# Use MB
			NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024" | bc) MB"
			
		elif [[ "${DU_SIZE}" -lt "1073741824" ]]; then
			
			# Use GB
			NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024^2" | bc) GB"
			
		elif [[ "${DU_SIZE}" -gt "1073741824" ]]; then
			
			# Use TB
			NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024^3" | bc) TB"
			
		fi
	
	elif [[ "${2}" = "KB" ]]; then
		
		NUMBER="$(echo "scale=2; ${DU_SIZE}" | bc) ${2}"
	
	elif [[ "${2}" = "MB" ]]; then
		
		NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024" | bc) ${2}"
	
	elif [[ "${2}" = "GB" ]]; then
		
		NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024^2" | bc) ${2}"
	
	elif [[ "${2}" = "TB" ]]; then
		
		NUMBER="$(echo "scale=2; ${DU_SIZE} / 1024^3" | bc) ${2}"
		
	fi
	
	echo "${NUMBER}"
	
}



DRIVE_USAGE () {

        # Function to check drive/disk usage.

        # Expected output:


        # Input arguments:
        # $1 = Mount point to check
        # $2 = Usage percentage alert level

        DRIVE="${1}"
        ALERT_PERCENT="${2}"

        STATS="$(df -H "${DRIVE}" | grep "${DRIVE}")"
        
        USED_SPACE="$(echo "${STATS}" | awk '{ print $3 }')"
        USED_PERCENT="$(echo "${STATS}" | awk '{ print $5 }')"
        DRIVE_SIZE="$(echo "${STATS}" | awk '{ print $2 }')"
        FREE_SPACE="$(echo "${STATS}" | awk '{ print $4 }')"
        DRIVE_MOUNT_POINT="$(echo "${STATS}" | awk '{ print $6 }')"

        echo "${DRIVE_MOUNT_POINT}"
        echo "${USED_SPACE} (${USED_PERCENT}) used of ${DRIVE_SIZE} (${FREE_SPACE} available)"
        echo ""

        USED_PERCENT_PLAIN=${USED_PERCENT//%/}
        FREE_PERCENT=$((100 - ${USED_PERCENT_PLAIN}))


        # If space used is equal to or higher than the alert amount
        if (("${USED_PERCENT_PLAIN}" >= "${ALERT_PERCENT}")); then
            
            echo "Alert! Only ${FREE_PERCENT}% remaining on ${DRIVE_MOUNT_POINT}."

            # Email
            TO_NAME="${MAILGUN_DEFAULT_RECEIVING_NAME}"
            TO_ADDRESS="${MAILGUN_DEFAULT_RECEIVING_ADDRESS}"

            FROM_NAME="${MAILGUN_DEFAULT_SENDING_NAME}"
            FROM_ADDRESS="${MAILGUN_DEFAULT_SENDING_ADDRESS}"

            SUBJECT="Drive Usage Alert: ${USED_PERCENT} used on ${DRIVE_MOUNT_POINT}"

            BODY_TEXT="${USED_SPACE} (${USED_PERCENT}) used of ${DRIVE_SIZE} (${FREE_SPACE} available)"


            MAILGUN "${TO_NAME}" "${TO_ADDRESS}" "${FROM_NAME}" "${FROM_ADDRESS}" "" "" "" "" "${SUBJECT}" "${BODY_TEXT}" "" ""

            PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${SUBJECT}" "${BODY_TEXT}"

        fi

}
