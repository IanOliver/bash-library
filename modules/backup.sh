#!/usr/bin/env bash

BACKUP_RDIFF () {
	
	# Function to perform an rdiff-backup of a directory.
	
	# Input arguments:
	# $1 = Source directory
	# $2 = Destination directory
	# $3 = Options and flags
	
	# Example usage:
	# BACKUP_RDIFF "/mnt/machine/directory/" "/backup/machine/directory/"
	# BACKUP_RDIFF "/mnt/machine/directory/" "/backup/machine/directory/" "--exclude-globbing-filelist \"/path/to/rdiff-exclude.txt\""
	
	# Establish variables
	local RDIFF_SOURCE="${1}"
	local RDIFF_DESTINATION="${2}"
	local RDIFF_OPTIONS="${3}"
	
	# Starting date and time
	local RDIFF_START_DATE="$(TIMESTAMP DATE_NOW)"
	local RDIFF_START_TIME="$(TIMESTAMP TIME_NOW_FULL)"
	local RDIFF_START_EPOCH="$(TIMESTAMP EPOCH)"
	
	echo ""
	echo ""
	echo "========================================================================="
	echo ""
	echo "Running rdiff-backup"
	echo ""
	echo "Source:          ${RDIFF_SOURCE}"
	echo "Destination:     ${RDIFF_DESTINATION}"
	echo "Start timestamp: ${RDIFF_START_DATE} at ${RDIFF_START_TIME}"
	echo ""
	echo "========================================================================="
	echo ""
	echo ""
	
	# If any options are passed
	if [[ ! -z "${RDIFF_OPTIONS}" ]]; then
		rdiff-backup -v5 --print-statistics --force "${RDIFF_OPTIONS}" "${RDIFF_SOURCE}" "${RDIFF_DESTINATION}"
	else
		rdiff-backup -v5 --print-statistics --force "${RDIFF_SOURCE}" "${RDIFF_DESTINATION}"
	fi
	
	# Ending and elapsed time
	local RDIFF_END_EPOCH="$(TIMESTAMP EPOCH)"
	local RDIFF_ELAPSED_TIME="$((${RDIFF_END_EPOCH} - ${RDIFF_START_EPOCH}))"
	
	echo "rdiff-backup took ${RDIFF_ELAPSED_TIME} seconds"
		
}


BACKUP_RDIFF_LOCAL_TO_REMOTE () {
	
	# Function to perform a remote (SSH-based) rdiff-backup of a directory.
	
	# Input arguments:
	# $1 = Backup name
	# $2 = Source directory
	# $3 = Destination directory
	# $4 = User
	# $5 = IP address/domain name
	# $6 = Port
	# $7 = Options and flags
	
	# Example usage:
	# BACKUP_RDIFF_LOCAL_TO_REMOTE "Name" "/mnt/machine/directory/" "/backup/machine/directory/" "username" "000.000.000.000" "999"
	
	# Example usage (with options)
	# BACKUP_RDIFF_LOCAL_TO_REMOTE "Name" "/mnt/machine/directory/" "/backup/machine/directory/" "username" "000.000.000.000" "999" "--exclude-globbing-filelist "/path/to/rdiff-exclude.txt""
	
	# Establish variables
	local RDIFF_NAME="${1}"
	local RDIFF_SOURCE="${2}"
	local RDIFF_DESTINATION="${3}"
	local RDIFF_USER="${4}"
	local RDIFF_ADDRESS="${5}"
	local RDIFF_PORT="${6}"
	local RDIFF_OPTIONS="${7}"
	
	# Starting date and time
	local RDIFF_START_DATE="$(TIMESTAMP DATE_NOW)"
	local RDIFF_START_TIME="$(TIMESTAMP TIME_NOW_FULL)"
	local RDIFF_START_EPOCH="$(TIMESTAMP EPOCH)"
	
	echo ""
	echo ""
	echo "========================================================================="
	echo ""
	echo "Running remote rdiff-backup"
	echo ""
	echo "Name:            ${RDIFF_NAME}"
	echo "Source:          ${RDIFF_SOURCE}"
	echo "Destination:     ${RDIFF_DESTINATION}"
	echo "User:            ${RDIFF_USER}"
	echo "Address:         ${RDIFF_ADDRESS}"
	echo "Port:            ${RDIFF_PORT}"
	echo "Options:         ${RDIFF_OPTIONS}"
	echo "Start timestamp: ${RDIFF_START_DATE} at ${RDIFF_START_TIME}"
	echo ""
	echo "========================================================================="
	echo ""
	echo ""
	
	# If any options are passed
	if [[ ! -z "${RDIFF_OPTIONS}" ]]; then
		rdiff-backup ${RDIFF_OPTIONS} --remote-schema "ssh -C -p ${RDIFF_PORT} %s rdiff-backup --server" -v5 --print-statistics --force "${RDIFF_SOURCE}" "${RDIFF_USER}@${RDIFF_ADDRESS}::${RDIFF_DESTINATION}"
	else
		rdiff-backup --remote-schema "ssh -C -p ${RDIFF_PORT} %s rdiff-backup --server" -v5 --print-statistics --force "${RDIFF_SOURCE}" "${RDIFF_USER}@${RDIFF_ADDRESS}::${RDIFF_DESTINATION}"
	fi
	
	# Ending and elapsed time
	local RDIFF_END_EPOCH="$(TIMESTAMP EPOCH)"
	local RDIFF_ELAPSED_TIME="$((${RDIFF_END_EPOCH} - ${RDIFF_START_EPOCH}))"
	
	echo "rdiff-backup took ${RDIFF_ELAPSED_TIME} seconds"
	
	# TODO: Add options/flags to command
	
}


BACKUP_RDIFF_REMOTE_TO_LOCAL () {
	
	# Function to perform a remote (SSH-based) rdiff-backup of a directory.
	
	# Input arguments:
	# $1 = Backup name
	# $2 = Source directory
	# $3 = Destination directory
	# $4 = User
	# $5 = IP address/domain name
	# $6 = Port
	# $7 = Options and flags
	
	# Example usage:
	# BACKUP_RDIFF "Name" "/mnt/machine/directory/" "/backup/machine/directory/" "username" "000.000.000.000" "999"
	# BACKUP_RDIFF "Name" "/mnt/machine/directory/" "/backup/machine/directory/" "username" "000.000.000.000" "999" "--exclude-globbing-filelist "/path/to/rdiff-exclude.txt""
	
	# Establish variables
	local RDIFF_NAME="${1}"
	local RDIFF_SOURCE="${2}"
	local RDIFF_DESTINATION="${3}"
	local RDIFF_USER="${4}"
	local RDIFF_ADDRESS="${5}"
	local RDIFF_PORT="${6}"
	local RDIFF_OPTIONS="${7}"
	
	# Starting date and time
	local RDIFF_START_DATE="$(TIMESTAMP DATE_NOW)"
	local RDIFF_START_TIME="$(TIMESTAMP TIME_NOW_FULL)"
	local RDIFF_START_EPOCH="$(TIMESTAMP EPOCH)"
	
	echo ""
	echo ""
	echo "========================================================================="
	echo ""
	echo "Running remote rdiff-backup"
	echo ""
	echo "Name:            ${RDIFF_NAME}"
	echo "Source:          ${RDIFF_SOURCE}"
	echo "Destination:     ${RDIFF_DESTINATION}"
	echo "User:            ${RDIFF_USER}"
	echo "Address:         ${RDIFF_ADDRESS}"
	echo "Port:            ${RDIFF_PORT}"
	echo "Options:         ${RDIFF_OPTIONS}"
	echo "Start timestamp: ${RDIFF_START_DATE} at ${RDIFF_START_TIME}"
	echo ""
	echo "========================================================================="
	echo ""
	echo ""
	
	# If any options are passed
	if [[ ! -z "${RDIFF_OPTIONS}" ]]; then
		rdiff-backup ${RDIFF_OPTIONS} --remote-schema "ssh -C -p ${RDIFF_PORT} %s rdiff-backup --server" -v5 --print-statistics --force "${RDIFF_USER}@${RDIFF_ADDRESS}::${RDIFF_SOURCE}" "${RDIFF_DESTINATION}"
	else
		rdiff-backup --remote-schema "ssh -C -p ${RDIFF_PORT} %s rdiff-backup --server" -v5 --print-statistics --force "${RDIFF_USER}@${RDIFF_ADDRESS}::${RDIFF_SOURCE}" "${RDIFF_DESTINATION}"
	fi
	
	# Ending and elapsed time
	local RDIFF_END_EPOCH="$(TIMESTAMP EPOCH)"
	local RDIFF_ELAPSED_TIME="$((${RDIFF_END_EPOCH} - ${RDIFF_START_EPOCH}))"
	
	echo "rdiff-backup took ${RDIFF_ELAPSED_TIME} seconds"
	
	# TODO: Add options/flags to command
	
}


CLEANUP_RDIFF () {
	
	# Function to perform a 'remove older than' on an existing rdiff-backup of a directory.
	
	# Input arguments:
	# $1 = Age of increments to keep
	# $2 = Directory
	
	# Example usage:
	# CLEANUP_RDIFF "1M" "/backup/machine/directory/"
	
	# Establish variables
	local RDIFF_AGE_TO_KEEP="${1}"
	local RDIFF_DIRECTORY="${2}"
	
	# Starting date and time
	local RDIFF_START_DATE="$(TIMESTAMP DATE_NOW)"
	local RDIFF_START_TIME="$(TIMESTAMP TIME_NOW_FULL)"
	local RDIFF_START_EPOCH="$(TIMESTAMP EPOCH)"
	
	echo ""
	echo ""
	echo "========================================================================="
	echo ""
	echo "Running rdiff-backup increment removal"
	echo ""
	echo "Directory:           ${RDIFF_DIRECTORY}"
	echo "Removing older than: ${RDIFF_AGE_TO_KEEP}"
	echo "Start timestamp:     ${RDIFF_START_DATE} at ${RDIFF_START_TIME}"
	echo ""
	echo "========================================================================="
	echo ""
	echo ""
	
	# Ending and elapsed time
	local RDIFF_END_EPOCH="$(TIMESTAMP EPOCH)"
	local RDIFF_ELAPSED_TIME="$((${RDIFF_END_EPOCH} - ${RDIFF_START_EPOCH}))"
	
	echo "rdiff-backup took ${RDIFF_ELAPSED_TIME} seconds"
	
	rdiff-backup -v5 --print-statistics --remove-older-than "${RDIFF_AGE_TO_KEEP}" --force "${RDIFF_DIRECTORY}"
	
}



BACKUP_CORE () {
	
		# TODO Check if rsync is installed
        
        # Function to backup key common system files to a given date-stamped directory
	
		# Input arguments:
		# $1 = Directory to store the backed-up files (without a trailing slash)
		
		# Example usage:
		# BACKUP_CORE "/path/to/backup"

		local BACKUP_LOCATION="${1}"
		
		if [[ ! -d "${BACKUP_LOCATION}" ]]; then
		
			# TODO Pushover alert
			exit 1
			
		else
			
			BACKUP_DESTINATION_PATH="${BACKUP_LOCATION}/$(TIMESTAMP DATE_NOW)_$(TIMESTAMP TIME_NOW_SHORT_FILENAME)"
		
			mkdir "${BACKUP_DESTINATION_PATH}"
			
			# Check if our new path exists
			if [[ -d "${BACKUP_DESTINATION_PATH}" ]]; then
		
				:
			
			else
				
				# No, it doesn't exist
				exit 1
				
			fi
		
		fi
		
		# Mini function to run each rsync
		BACKUP_CORE_CMD () {
			
			local BACKUP_SOURCE="${1}"
			local CONTAINING_DIR="${2}"
						
	        # Check for existence of file/directory
	        if [[ -e "${BACKUP_SOURCE}" ]]; then
	        	
	        		mkdir -p "${BACKUP_DESTINATION_PATH}/${CONTAINING_DIR}/"

	                rsync -r -t -v --quiet --progress --delete-excluded -s -h -z "${BACKUP_SOURCE}" "${BACKUP_DESTINATION_PATH}/${CONTAINING_DIR}/"
	        else
	                echo "${BACKUP_SOURCE} does not exist, skipping..."
	        fi
        
    	}
		
		
		# System
		BACKUP_CORE_CMD "/etc/exports"												"etc"
		BACKUP_CORE_CMD "/etc/fstab"												"etc"
		BACKUP_CORE_CMD "/etc/hosts"												"etc"
		BACKUP_CORE_CMD "/etc/samba/smb.conf"										"etc/samba"
		BACKUP_CORE_CMD "/etc/ssh/sshd_config"										"etc/ssh"

		# Scripting
		BACKUP_CORE_CMD "${MACHINE_SCRIPTS_DIR}/"             						"scripts/"

		# Syncthing
		BACKUP_CORE_CMD "/home/${USER}/.config/syncthing/config.xml"				"syncthing"
		BACKUP_CORE_CMD "/home/${USER}/.config/syncthing/cert.pem"					"syncthing"
		BACKUP_CORE_CMD "/home/${USER}/.config/syncthing/key.pem"					"syncthing"

		# Home
		BACKUP_CORE_CMD "/home/${USER}/.bashrc"										"home"
		BACKUP_CORE_CMD "/home/${USER}/.bash_aliases"								"home"
		BACKUP_CORE_CMD "/home/${USER}/.bash_history"								"home"
		BACKUP_CORE_CMD "/home/${USER}/.gtk-bookmarks"								"home"
		BACKUP_CORE_CMD "/home/${USER}/.gitconfig"									"home"

		# SSH
		BACKUP_CORE_CMD "/home/${USER}/.ssh/"										"home/ssh"

		# Logs
		BACKUP_CORE_CMD "/var/log/auth.log"											"var/log"
		BACKUP_CORE_CMD "/var/log/boot.log"											"var/log"
		BACKUP_CORE_CMD "/var/log/kern.log"											"var/log"
		BACKUP_CORE_CMD "/var/log/syslog"											"var/log"
		BACKUP_CORE_CMD "/var/log/apt/history.log"									"var/log/apt"
		BACKUP_CORE_CMD "/var/log/apt/term.log"										"var/log/apt"
		BACKUP_CORE_CMD "/var/log/dist-upgrade/apt.log"								"var/log/dist-upgrade"
		BACKUP_CORE_CMD "/var/log/dist-upgrade/apt-term.log"						"var/log/dist-upgrade"
		BACKUP_CORE_CMD "/var/log/dist-upgrade/history.log"							"var/log/dist-upgrade"
		BACKUP_CORE_CMD "/var/log/unattended-upgrades/unattended-upgrades.log"		"var/log/unattended-upgrades"
		BACKUP_CORE_CMD "/var/log/unattended-upgrades/unattended-upgrades-dpkg.log"	"var/log/unattended-upgrades"
		
		# Logitech Media Server
		BACKUP_CORE_CMD "/var/lib/squeezeboxserver/prefs/server.prefs"				"logitech-media-server"
		BACKUP_CORE_CMD "/var/lib/squeezeboxserver/prefs/favorites.opml"			"logitech-media-server"
		BACKUP_CORE_CMD "/var/lib/squeezeboxserver/prefs/plugin"					"logitech-media-server"
		
		# CouchPotato
		BACKUP_CORE_CMD "/home/${USER}/.couchpotato/settings.conf"							"couchpotato"
		BACKUP_CORE_CMD "/home/${USER}/.couchpotato/database"								"couchpotato"
		BACKUP_CORE_CMD "/home/${USER}/.couchpotato/db_backup"								"couchpotato"

		#NZBget
		BACKUP_CORE_CMD "/home/${USER}/nzbget/nzbget.conf"									"nzbget"

		#Sonarr
		BACKUP_CORE_CMD "/home/${USER}/.config/NzbDrone/Backups/scheduled"					"sonarr"
		

		# Crontab	
		if [[ ! $(crontab -l | wc -c) -eq 0 ]]; then
			crontab -l > "${BACKUP_DESTINATION_PATH}/crontab.txt"
		fi
		
}
