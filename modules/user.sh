#!/usr/bin/env bash

REQUIRE_ROOT () {
	echo "Root required..."
	sudo echo "Root confirmed..."
}

REQUIRE_ROOT_AUTO () {
	if [[ $EUID != 0 ]]; then
		echo "Please run with root privileges."
		echo "Exiting script."
		exit 1
	fi
}