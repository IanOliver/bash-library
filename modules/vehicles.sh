function DVLA () {

	# Function to check the MOT and Tax status of a given registration number, and alert if either expire within a threshold date

	# Required software:
	# curl
	# jq

	# Required variables:
	# EXPIRY_THRESHOLD_DATE (usually a date in the future from today, e.g. today + 14 days, in YYYY-MM-DD format)
	# TO_NAME
	# TO_ADDRESS
	# FROM_NAME
	# FROM_ADDRESS
	# PUSHOVER_USER
	# PUSHOVER_TOKEN

	# Input arguments:
	# $1 = Vehicle registration number (no spaces, e.g. AB08CDE)

	local VEHICLE_REG=${1}

	CURL_COMMAND="curl -s -L -X POST 'https://driver-vehicle-licensing.api.gov.uk/vehicle-enquiry/v1/vehicles' -H 'x-api-key: ${DVLA_API_KEY}' -H 'Content-Type: application/json' -d '{\"registrationNumber\": \"${VEHICLE_REG}\"}'"

	# Run cURL
	DATA=$(echo "${CURL_COMMAND}" | sh)

	# Preview response
	#echo "${DATA}"

	VEHICLE_MAKE=$(echo "${DATA}" | jq -r '.make')
	VEHICLE_YEAR=$(echo "${DATA}" | jq -r '.yearOfManufacture')
	VEHICLE_COLOUR=$(echo "${DATA}" | jq -r '.colour')
	VEHICLE_ENGINE_CC=$(echo "${DATA}" | jq -r '.engineCapacity')
	VEHICLE_FUEL_TYPE=$(echo "${DATA}" | jq -r '.fuelType')
	MOT_STATUS=$(echo "${DATA}" | jq -r '.motStatus')
	MOT_EXPIRY=$(echo "${DATA}" | jq -r '.motExpiryDate')
	TAX_STATUS=$(echo "${DATA}" | jq -r '.taxStatus')
	TAX_EXPIRY=$(echo "${DATA}" | jq -r '.taxDueDate')


	echo "Vehicle registration number: ${VEHICLE_REG}"
	echo "Vehicle details: ${VEHICLE_YEAR} ${VEHICLE_MAKE} (${VEHICLE_COLOUR} ${VEHICLE_ENGINE_CC} ${VEHICLE_FUEL_TYPE})"

	echo "MOT Status: ${MOT_STATUS}"
	echo "MOT Expiry: ${MOT_EXPIRY}"

	echo "Tax Status: ${TAX_STATUS}"
	echo "Tax Expiry: ${TAX_EXPIRY}"


	# If we are within our MOT expiry threshold window
	# E.g. if 2023-12-02 < 2023-12-03 (two weeks from 2023-12-19)
	if [[ "${MOT_EXPIRY}" < "${EXPIRY_THRESHOLD_DATE}" ]]; then
			
		SUBJECT="MOT for ${VEHICLE_REG} expires on ${MOT_EXPIRY}"
		BODY_TEXT="MOT for ${VEHICLE_REG} (${VEHICLE_YEAR} ${VEHICLE_MAKE} ${VEHICLE_COLOUR} ${VEHICLE_ENGINE_CC} ${VEHICLE_FUEL_TYPE}) expires on ${MOT_EXPIRY} and needs renewing!"

		echo "${BODY_TEXT}"
		
		MAILGUN	"${TO_NAME}" "${TO_ADDRESS}" "${FROM_NAME}" "${FROM_ADDRESS}" "" "" "" "" "${SUBJECT}" "${BODY_TEXT}" "" ""
		
		PUSHOVER "${PUSHOVER_IAN_USER}" "${PUSHOVER_IAN_TOKEN}" "${SUBJECT}" "${BODY_TEXT}"

	fi

	# If we are within our Tax expiry threshold window
	# E.g. if 2023-12-02 < 2023-12-03 (two weeks from 2023-12-19)
	if [[ "${TAX_EXPIRY}" < "${EXPIRY_THRESHOLD_DATE}" ]]; then
	
		SUBJECT="Tax for ${VEHICLE_REG} expires on ${TAX_EXPIRY}"
		BODY_TEXT="Tax for ${VEHICLE_REG} (${VEHICLE_YEAR} ${VEHICLE_MAKE} ${VEHICLE_COLOUR} ${VEHICLE_ENGINE_CC} ${VEHICLE_FUEL_TYPE}) expires on ${TAX_EXPIRY} and needs renewing!"

		echo "${BODY_TEXT}"
		
		MAILGUN	"${TO_NAME}" "${TO_ADDRESS}" "${FROM_NAME}" "${FROM_ADDRESS}" "" "" "" "" "${SUBJECT}" "${BODY_TEXT}" "" ""
		
		PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "${SUBJECT}" "${BODY_TEXT}"

	fi

}