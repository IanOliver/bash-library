#!/usr/bin/env bash

PI_HOLE_UPDATE_SUBSCRIPTIONS() {
	
	# This function:
	#	- Downloads an array of domain lists or blocklists
	#   - Compares the downloaded lists to previously-downloaded copies
	#   - If differences are found:
	#		- The lists are concatenated
	#       - The concatenated list is tidied
	#       - Each line of the concatenated list is inserted into the Pi-hole domainlist database
	#       - Pihole is reloaded as necessary (either gravity or restartdns)
	
	SCRIPT_LOCK_START "${PI_HOLE_LIST_UPDATE_LOCK}"
	
	LIST_TYPE="${1}"
	
	# Delete the first argument with shift, then use the other arguments as array items
	shift
	LIST_ARRAY=("$@")
	
	
	echo "LIST_ARRAY: ${LIST_ARRAY}"
	
	# Debugging
	# echo "Array: ${LIST_ARRAY[*]}"
	# for LIST_URL in "${!LIST_ARRAY[@]}"
	# do
	# 	echo "ID ${LIST_URL}: ${LIST_ARRAY[$LIST_URL]}"
	# done
	# echo -e "${LISTS_DOWNLOAD_DIR}"
	# echo -e "${LISTS_DOWNLOAD_NEW_DIR}"
	# echo -e "${LISTS_DOWNLOAD_PREV_DIR}"

	# Set default state
	local LISTS_DIFFER=false
	
	
	# Establish download diretories
	local LISTS_DOWNLOAD_DIR="${PI_HOLE_DOWNLOAD_DIR}/${LIST_TYPE}"
	
	local LISTS_DOWNLOAD_NEW_DIR="${LISTS_DOWNLOAD_DIR}/new"
	local LISTS_DOWNLOAD_PREV_DIR="${LISTS_DOWNLOAD_DIR}/prev"

	local CONCATENATED_LIST_FILE="${LISTS_DOWNLOAD_DIR}/${LIST_TYPE}_concatenated.txt"
	
	
	# ======================================================
	# Check if the download directories exist


	echo -e "Checking if download directory and subdirectories exist..."

	if [[ -d "${LISTS_DOWNLOAD_DIR}" ]] && [[ -d "${LISTS_DOWNLOAD_NEW_DIR}" ]] && [[ -d "${LISTS_DOWNLOAD_PREV_DIR}" ]]; then
		echo -e "Download directories exist; continuing..."
		local LISTS_DOWNLOAD_DIRS_EXISTS=true
	else
		echo -e "One or more download of the directories does not exist; making them..."
		
		# Make the directory
		mkdir -p "${LISTS_DOWNLOAD_DIR}"
		mkdir -p "${LISTS_DOWNLOAD_NEW_DIR}"
		mkdir -p "${LISTS_DOWNLOAD_PREV_DIR}"
		
		# Check again
		if [[ -d "${LISTS_DOWNLOAD_DIR}" ]] && [[ -d "${LISTS_DOWNLOAD_NEW_DIR}" ]] && [[ -d "${LISTS_DOWNLOAD_PREV_DIR}" ]]; then
			echo -e "Download directories now exist; continuing..."
			local LISTS_DOWNLOAD_DIRS_EXISTS=true
		else
			echo -e "One or more of the download directories was not/cannot be made; aborting..."
			if [[ "${NOTIFY_ON_FAILURE}" = true ]]; then
				echo -e "Sending notification..."
				PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} Failure" "${MACHINE_NAME} on ${NETWORK_NAME} network failed making download directories"
			fi
			
			SCRIPT_LOCK_FINISH "${PI_HOLE_LIST_UPDATE_LOCK}"
			
			exit 1
		fi

	fi
	
	# ======================================================
	# Download the allowlist(s)


	if [[ "${LISTS_DOWNLOAD_DIRS_EXISTS}" = true ]]; then

		echo -e "Downloading list(s)..."

		# ! denotes using the array index number instead of value
		for LIST_URL in "${!LIST_ARRAY[@]}"
		do
			
			echo -e '\n'
			echo -e "========================================================================"
			echo -e '\n'
			
			# Display allowlist array index and value
			echo "${LIST_URL}: ${LIST_ARRAY[$LIST_URL]}"
			
			# Establish prev and new filenames/paths
			local PREV_FILE="${LISTS_DOWNLOAD_PREV_DIR}/prev_${LIST_URL}.txt"
			local NEW_FILE="${LISTS_DOWNLOAD_NEW_DIR}/new_${LIST_URL}.txt"

			# Check if a previously downlaoded allowlist exists
			if [[ -f "${NEW_FILE}" ]]; then
				
				# This allowlist has already been downloaded in the past
				echo "Previous version of list found"
				
				# Move what were the new files last time, to now be the 'prev' files
				echo "Moving previously-downloaded version of list"
				mv "${NEW_FILE}" "${PREV_FILE}"
				
			else
				
				# This list has not been downloaded in the past, it must be a new one
				echo "No previously-downloaded version of list was found, this must be a new list"
				
			fi
			
			# Download the new file
			echo "Downloading the list..."
			
			local CURL_URL="${LIST_ARRAY[$LIST_URL]}"
			local CURL_OUTPUT="${NEW_FILE}"
			
			# Debug
			echo "CURL_URL: ${CURL_URL}"
			echo "CURL_OUTPUT: ${CURL_OUTPUT}"
			
			if curl -L "${CURL_URL}" -o "${CURL_OUTPUT}"
			then
				
				echo -e "List downloaded; continuing..."
				local URL_DOWNLOADED=true
				
			fi
				
			
			# ======================================================
			# Check the downloaded file actually, definitely exists
					

			if [[ "${URL_DOWNLOADED}" = true ]]; then
				
				echo -e "Checking if downloaded allowlist (${NEW_FILE}) defintely exists locally..."

				if [[ -f "${NEW_FILE}" ]]; then
					echo -e "${NEW_FILE} exists; continuing..."
					local URL_DOWNLOAD_EXISTS=true
				else
					echo -e "${NEW_FILE} doesn't exist; aborting..."
					if [[ "${NOTIFY_ON_FAILURE}" = true ]]; then
						echo -e "Sending notification..."
						PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} Failure" "${MACHINE_NAME} on ${NETWORK_NAME} network failed: downloaded list (${NEW__FILE}) doesn't exist"
					fi
					
					SCRIPT_LOCK_FINISH "${PI_HOLE_LIST_UPDATE_LOCK}"
					
					exit 1
				fi
					
			fi
			
			# ======================================================
			# Compare old and new downloaded lists	
					
			if [[ "${URL_DOWNLOAD_EXISTS}" = true ]]; then
				
				# Append a new line at the end of the file
				# This prevents any ending lines merging with beginning lines of lists
				echo -e "" >> "${NEW_FILE}"
				
				# Check if a previously downloaded old allowlist exists
				if [[ -f "${PREV_FILE}" ]]; then
					
					# We have an old version, and can compare it with the just-downloaded version
					if diff "${NEW_FILE}" "${PREV_FILE}" >/dev/null; then
					
						# Files are the same
						echo "No differences between old and new versions"
						
					else
						
						# Files are different
						local LISTS_DIFFER=true
						echo "Differences found between old and new versions"
						
					fi
					
				else
					
					# We do not have an old version of this file, it must be a new one
					local LISTS_DIFFER=true
					echo "No previous version of list to compare against as this is a new list. We will update the database."
					
				fi

			else
				
				echo -e "List download failed; aborting..."
				if [[ "${NOTIFY_ON_FAILURE}" = true ]]; then
					echo -e "Sending notification..."
					PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} Failure" "${MACHINE_NAME} on ${NETWORK_NAME} network failed downloading URL: ${ALLOWLISTS[$ALLOWLIST_URL]}"
				fi
				
				SCRIPT_LOCK_FINISH "${PI_HOLE_LIST_UPDATE_LOCK}"
				
				exit 1

			fi

		# End of looping
		done

	fi
	
	
	echo -e '\n'
	echo -e "========================================================================"
	echo -e '\n'
	
	# Testing
	# LISTS_DIFFER=true


	if [[ "${LISTS_DIFFER}" = true ]] || [[ "${FORCE_UPDATE}" = true ]]; then
		
		echo "One or more lists differ"
		
		# Concatenate the downloaded list
		echo -e "Concatenating lists..."
		cat "${LISTS_DOWNLOAD_NEW_DIR}"/*.txt > "${CONCATENATED_LIST_FILE}"
		# TRY: for i in *.txt;do cat $i >> output.txt;done

		echo -e "Convert concatenated list to use UNIX line endings..."
		awk 'BEGIN{RS="\1";ORS="";getline;gsub("\r","");print>ARGV[1]}' "${CONCATENATED_LIST_FILE}"

		echo -e "Removing empty lines from concatenated list..."	
		sed -i '/^$/d' "${CONCATENATED_LIST_FILE}"

		echo -e "Removing comment lines from concatenated list..."
		sed -i '/^#/d' "${CONCATENATED_LIST_FILE}"

		echo -e "Sorting lines..."
		# sort "${CONCATENATED_ALLOWLIST_FILE}" | uniq -u
		
		echo -e "Removing duplicate lines..."
		awk '!seen[$0]++' "${CONCATENATED_LIST_FILE}" > "${LISTS_DOWNLOAD_DIR}/"tmp.txt
		
		echo -e "Moving temp file..."
		mv "${LISTS_DOWNLOAD_DIR}"/tmp.txt "${CONCATENATED_LIST_FILE}"
		
		local CONCATENATED_LIST_TIDIED=true
		
	elif [[ "${LISTS_DIFFER}" = false ]]; then
		
		echo "No lists differ"
		
		# No need to do anything
		echo "Exiting script"
		
		SCRIPT_LOCK_FINISH "${PI_HOLE_LIST_UPDATE_LOCK}"
		
		exit 0

	fi
	
	
	if [[ "${CONCATENATED_LIST_TIDIED}" = true ]]; then

		# ==============================================================
		# SQL
		# ==============================================================
		
		# Domain type
		if [[ "${LIST_TYPE}" = "allowlist-exact" ]]; then
			local DOMAIN_TYPE=0
		elif [[ "${LIST_TYPE}" = "allowlist-regex" ]]; then
			local DOMAIN_TYPE=2
		elif [[ "${LIST_TYPE}" = "blocklist-regex" ]]; then
			local DOMAIN_TYPE=3
		fi

		# Database name and labels
		if \
			[[ "${LIST_TYPE}" = "allowlist-exact" ]] || \
			[[ "${LIST_TYPE}" = "allowlist-regex" ]] || \
			[[ "${LIST_TYPE}" = "blocklist-regex" ]]; then
				
			local DATABASE_NAME="domainlist"
			local DATABASE_LABELS="type,domain,enabled"
			local DELETE_SQL_STATEMENT="sudo sqlite3 /etc/pihole/gravity.db \"delete from ${DATABASE_NAME} where type=${DOMAIN_TYPE};\""
			local RELOAD_COMMAND="pihole restartdns"
			
		elif [[ "${LIST_TYPE}" = "blocklists" ]]; then
			
			local DATABASE_NAME="adlist"
			local DATABASE_LABELS="address,enabled"
			local DELETE_SQL_STATEMENT="sudo sqlite3 /etc/pihole/gravity.db \"delete from ${DATABASE_NAME};\""
			local RELOAD_COMMAND="pihole -g"
			
		fi

		


		# Begin new SQL statement
		local NEW_SQL_STATEMENT="sudo sqlite3 /etc/pihole/gravity.db \"insert or REPLACE into ${DATABASE_NAME} (${DATABASE_LABELS}) values "


		# Add values to SQL statement
		if \
			[[ "${LIST_TYPE}" = "allowlist-exact" ]] || \
			[[ "${LIST_TYPE}" = "allowlist-regex" ]] || \
			[[ "${LIST_TYPE}" = "blocklist-regex" ]]; then
				
			# Add each domain to the statement
			# -r leave backslash escapes un-evaluated, which is needd for regex expressions
			while read -r LINE; do
				local NEW_SQL_STATEMENT+="(${DOMAIN_TYPE},'${LINE}',1),"
			done <"${CONCATENATED_LIST_FILE}"

		elif [[ "${LIST_TYPE}" = "blocklists" ]]; then
			
			# Add each list to the statement
				while read LINE; do
					local NEW_SQL_STATEMENT+="('${LINE}',1),"
				done <"${CONCATENATED_LIST_FILE}"
			
		fi


		# Trim from the trailing comma
		local NEW_SQL_STATEMENT="${NEW_SQL_STATEMENT%,}"

		# Complete statement
		local NEW_SQL_STATEMENT+=";\""
		
		# Preview delete SQL statement
		echo "${DELETE_SQL_STATEMENT}"

		# Preview new SQL statement
		echo "${NEW_SQL_STATEMENT}"
		
		
		
		
		

		# Execute delete SQL statement
		echo -e "Deleting existing list entries..."
		echo "${DELETE_SQL_STATEMENT}" | bash
		
		# Execute new SQL statement
		echo -e "Overwriting existing list entries..."
		echo "${NEW_SQL_STATEMENT}" | bash
		
		# Run reload/update command
		echo -e "Reloading..."
		echo "${RELOAD_COMMAND}" | bash

		

	fi


	# ======================================================
	# Complete


	if [ $? -eq 0 ]; then
	    echo -e "${OPERATION} complete!"
	else
	    PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "${OPERATION} Failure" "${MACHINE_NAME} on ${NETWORK_NAME} network failed inserting into database"
	    :
	fi

	SCRIPT_LOCK_FINISH "${PI_HOLE_LIST_UPDATE_LOCK}"

	# ======================================================
	# Exit
	
	return
	
}



PI_HOLE_PROVISION_WEBSERVER_HTTPS() {
	
	REQUIRE_ROOT
	
	# Ask what domain name is to be used
	echo "What domain name is to be used (e.g. pi-hole.domain.com)? "
	read userInput
	if [[ -n "${userInput}" ]]; then
	    CERT_DOMAIN_NAME="${userInput}"
	fi

	# Preview
	echo "Certificate to use: ${CERT_DOMAIN_NAME}"

	# Confirm if correct
	while true; do
	    read -p "Is this correct? (y/n) " yn
	    case $yn in
	        [Yy]* ) CONTINUE=true; break;;
	        [Nn]* ) exit;;
	        * ) echo "Please answer y or no.";;
	    esac
	done
	
	# Grab the conf file contents
	CONF_FILE_CONTENTS="$(cat "/home/ian/scripts/bash-library/resource/pi-hole_lighttpd.conf")"
	
	# Replace the placeholder domain name, cert path and CA file path with the one to use
	CONF_FILE_CONTENTS="$(echo "${CONF_FILE_CONTENTS}" | sed 's/VAR_DOMAIN_NAME/'"${CERT_DOMAIN_NAME}"'/g')"
	# Using alternative sed separator dues to forward slashes in variable
	CONF_FILE_CONTENTS="$(echo "${CONF_FILE_CONTENTS}" | sed 's:VAR_DIR:'"${HOME}/pi-hole/certificates"':g')"
	CONF_FILE_CONTENTS="$(echo "${CONF_FILE_CONTENTS}" | sed 's/VAR_CERT_FILE/'"${CERT_DOMAIN_NAME}.combined.pem"'/g')"
	CONF_FILE_CONTENTS="$(echo "${CONF_FILE_CONTENTS}" | sed 's/VAR_CA_FILE/'"${CERT_DOMAIN_NAME}.combined.pem"'/g')"

	# Insert the updated conf into the file	
	echo "${CONF_FILE_CONTENTS}" | sudo tee /etc/lighttpd/external.conf > /dev/null

	
}



# See https://docs.pi-hole.net/guides/dns/cloudflared/#updating-cloudflared
# See https://developers.cloudflare.com/cloudflare-one/connections/connect-apps/install-and-setup/installation
# See https://github.com/cloudflare/cloudflared/releases/latest/

CLOUDFLARED_LATEST_RELEASE_PAGE="https://github.com/cloudflare/cloudflared/releases/latest/"

PI_HOLE_CLOUDFLARED_INSTALL_BINARY() {
	
	
	if [[ "${MACHINE_ARCHITECTURE}" = "amd64" ]]; then
					
		local DOWNLOAD_URL="https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-amd64.deb"
		local DOWNLOAD_FILENAME="cloudflared-stable-linux-amd64_dl-$(TIMESTAMP DATE_NOW).deb"
		
		# Perform update
		wget "${DOWNLOAD_URL}" -O "${DOWNLOAD_FILENAME}"
		
		# Check if cloudflared is already installed
		if [[ $(dpkg-query -W -f='${Status}' cloudflared 2>/dev/null | grep -c "ok installed") -eq 1 ]] || \
			[[ $(cloudflared -v 2>/dev/null | grep -c "(built") -eq 1 ]]; then

			# Stop the service
			sudo systemctl stop cloudflared

		fi
		
		sudo apt-get install ./"${DOWNLOAD_FILENAME}"

	elif [[ "${MACHINE_ARCHITECTURE}" = "arm64" ]]; then
		
		local DOWNLOAD_URL="https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-arm64"
		local DOWNLOAD_FILENAME="cloudflared_dl-$(TIMESTAMP DATE_NOW)"
		
		# Perform update
		echo "Downloading cloudflared..."
		wget "${DOWNLOAD_URL}" -O "${DOWNLOAD_FILENAME}"
		
		# Check if cloudflared is already installed
		if [[ $(dpkg-query -W -f='${Status}' cloudflared 2>/dev/null | grep -c "ok installed") -eq 1 ]] || \
			[[ $(cloudflared -v 2>/dev/null | grep -c "(built") -eq 1 ]]; then

			# Stop the service
			sudo systemctl stop cloudflared

		fi
		
		echo "Moving cloudflared..."
		sudo mv "${DOWNLOAD_FILENAME}" /usr/local/bin/cloudflared
		
		echo "Making cloudflared executable..."
		sudo chmod +x /usr/local/bin/cloudflared
		
		echo "Displaying cloudflared version..."
		cloudflared -v
		
		return

	elif [[ "${MACHINE_ARCHITECTURE}" = "arm32" ]]; then
		
		#local DOWNLOAD_URL="https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-arm.tgz"
		local DOWNLOAD_URL="https://github.com/cloudflare/cloudflared/releases/download/${CLOUDFLARED_LATEST_RELEASE_VERSION}/cloudflared-linux-arm"
		#local DOWNLOAD_FILENAME="cloudflared_dl-$(TIMESTAMP DATE_NOW).tgz"
		local DOWNLOAD_FILENAME="cloudflared-linux-arm_${CLOUDFLARED_LATEST_RELEASE_VERSION}"
		
		# Perform update
		echo "Downloading cloudflared..."
		wget "${DOWNLOAD_URL}" -O "${DOWNLOAD_FILENAME}"
		
		# Check if cloudflared is already installed
		if [[ $(dpkg-query -W -f='${Status}' cloudflared 2>/dev/null | grep -c "ok installed") -eq 1 ]] || \
			[[ $(cloudflared -v 2>/dev/null | grep -c "(built") -eq 1 ]]; then

			# Stop the service
			sudo systemctl stop cloudflared

		fi
		
		# echo "Extracting cloudflared..."
		# tar -xvzf "${DOWNLOAD_FILENAME}"
		
		echo "Copying cloudflared..."
		#sudo cp ./cloudflared /usr/local/bin
		sudo cp ./"${DOWNLOAD_FILENAME}" /usr/local/bin/cloudflared
		
		echo "Making cloudflared executable..."
		sudo chmod +x /usr/local/bin/cloudflared
		
		echo "Displaying cloudflared version..."
		cloudflared -v
		
		echo "Removing downloaded file..."
		rm ./"${DOWNLOAD_FILENAME}"
		
		return
		
	else
		echo "Machine architecture declared incorrectly or not declared; exiting."
		exit 1
	fi

}



PI_HOLE_CLOUDFLARED_ADD_STARTUP() {
	
	# Create a cloudflared user to run the daemon:
	sudo useradd -s /usr/sbin/nologin -r -M cloudflared
	
	# Create a configuration file for cloudflared
	
	# Grab the command line file contents
	local COMMAND_LINE_OPTIONS="$(cat "$HOME/scripts/bash-library/resource/cloudflared-command-line-options")"
	
	# If a Cloudflare Teams subdomain is specified, use it
	if [[ ! -z "${PI_HOLE_CLOUDFLARE_TEAMS_SUBDOMAIN}" ]]; then
	COMMAND_LINE_OPTIONS="$(echo "${COMMAND_LINE_OPTIONS}" | sed -b "s/--upstream https:\/\/1.1.1.1\/dns-query --upstream https:\/\/1.0.0.1\/dns-query/--upstream https:\/\/${PI_HOLE_CLOUDFLARE_TEAMS_SUBDOMAIN}.cloudflare-gateway.com\/dns-query/")"	
	fi
	
	# Insert the command line options
	echo "${COMMAND_LINE_OPTIONS}" | sudo tee /etc/default/cloudflared > /dev/null
	
	# Update the permissions for the configuration file and cloudflared binary to allow access for the cloudflared user
	sudo chown cloudflared:cloudflared /etc/default/cloudflared
	sudo chown cloudflared:cloudflared /usr/local/bin/cloudflared
	
	# Grab the systemd config file contents
	local SYSTEMD_CONFIG="$(cat "$HOME/scripts/bash-library/resource/cloudflared-systemd-config")"
	
	# Insert the config
	echo "${SYSTEMD_CONFIG}" | sudo tee /etc/systemd/system/cloudflared.service > /dev/null
	
	# Enable the service
	sudo systemctl enable cloudflared
	
	# Start the service
	sudo systemctl start cloudflared
	
	# Check the status of the service
	sudo systemctl status cloudflared
	
}



PI_HOLE_CLOUDFLARED_INSTALL_UPDATE() {
	
	# Find out the latest release by getting the 302 redirect
	# https://unix.stackexchange.com/questions/45325/get-urls-redirect-target-with-curl/317657
	local CLOUDFLARED_LATEST_RELEASE_302="$(curl -w "%{url_effective}\n" -I -L -s -S "${CLOUDFLARED_LATEST_RELEASE_PAGE}" -o /dev/null)"
	local CLOUDFLARED_LATEST_RELEASE_VERSION="${CLOUDFLARED_LATEST_RELEASE_302##*/}"
		
	# Find out if cloudflared is already installed (https://stackoverflow.com/a/22592801)
	if [[ $(dpkg-query -W -f='${Status}' cloudflared 2>/dev/null | grep -c "ok installed") -eq 1 ]] || \
		[[ $(cloudflared -v 2>/dev/null | grep -c "(built") -eq 1 ]]; then
		
		# cloudflared is already installed
		
		# Get the currently-installed version
		local CLOUDFLARED_CURRENTLY_INSTALLED_VERSION="$(cloudflared -v)"
		local CLOUDFLARED_CURRENTLY_INSTALLED_VERSION="$(echo ${CLOUDFLARED_CURRENTLY_INSTALLED_VERSION} | awk '{print $3}')"
		
		echo "'cloudflared' is already installed (version ${CLOUDFLARED_CURRENTLY_INSTALLED_VERSION})."
		echo "Latest version available is ${CLOUDFLARED_LATEST_RELEASE_VERSION}."
		
		# Find out if cloudflared needs updating
		
		# Compare versions
		if [[ "${CLOUDFLARED_CURRENTLY_INSTALLED_VERSION}" == "${CLOUDFLARED_LATEST_RELEASE_VERSION}" ]]; then
			echo "No need to update, exiting"
			exit 0
		else
			echo "Update is required"
			
			# Install cloudflared over the top
			PI_HOLE_CLOUDFLARED_INSTALL_BINARY
			
			# Start the service
			sudo systemctl start cloudflared
			
		fi

	else
		
		# cloudflared is not installed
		
		echo "'cloudflared' is not installed, installing..."
		
		echo "Latest version available is ${CLOUDFLARED_LATEST_RELEASE_VERSION}."
		
		# Install cloudflared
		PI_HOLE_CLOUDFLARED_INSTALL_BINARY
		
		# Start the service
		sudo systemctl start cloudflared
		
		# Configure to run on startup
		# PI_HOLE_CLOUDFLARED_ADD_STARTUP
		
	fi

}
