#!/usr/bin/env bash

SSH_SHUTDOWN() {
	
	# Function to shutdown another machine via SSH
	
	# Recommendations:
	# To prevent the sudo password prompt when using shutdown, the machine should be provisioned ahead of time, using:
	# sudo visudo
	# Then adding either (all users):
	# %users ALL=(ALL) NOPASSWD: /sbin/poweroff, /sbin/reboot, /sbin/shutdown
	# Or (speficic user):
	# [username] ALL=(ALL) NOPASSWD: /sbin/poweroff, /sbin/reboot, /sbin/shutdown
		
	# Input arguments:
	# $1 = IP address
	# $2 = User
	
	# Example usage:
	# SSH_SHUTDOWN "192.168.1.x" "username"
	
	IP_ADDRESS=${1}
	USER=${2}
	
	ssh -o ConnectTimeout=5 "${USER}@${IP_ADDRESS}" 'sudo /sbin/shutdown -h now'
	
}


WOL () {
	wakeonlan "${1}"
}


WOL_DATE () {
	if [[ "${1}" != *"$(TIMESTAMP DATE_NOW)"* ]]; then
		WOL "${2}"
	fi
}
