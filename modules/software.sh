#!/usr/bin/env bash

PFSENSE_GET_CONFIG () {
	
	# Function to copy the current in-use pfSense config to another local directory
	
	# Required variables:
	# MACHINE_DEFAULT_SSH_IDENTITY
	
	# Input arguments:
	# $1 = pfSense user
	# $2 = pfSense IP address
	# $3 = Port number for remote machine SSH (defaults to 22)
	# $4 = pfSense nickname
	# $5 = Directory to store the backup config file
	
	# Example usage:
	# PFSENSE_GET_CONFIG "admin" "192.168.1.1" "home-sg-1100" "/path/to/pfsense/config-backup"
	
	local PFSENSE_USER=${1}
	local PFSENSE_IP=${2}
	local PORT_NUMBER=${3}
	local PFSENSE_NICKNAME=${4}
	local STORAGE_DIRECTORY=${5}
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	# TODO Check download directory exists
	
	echo "${MACHINE_DEFAULT_SSH_IDENTITY}"
	# Get the config file
	/usr/bin/scp -i "${MACHINE_DEFAULT_SSH_IDENTITY}" -P "${PORT_NUMBER}" "${PFSENSE_USER}"@"${PFSENSE_IP}":"/cf/conf/config.xml" "${STORAGE_DIRECTORY}/pfsense_${PFSENSE_NICKNAME}_config_backup_$(date +"%F")_$(date +"%H-%M-%S").xml"
	
	# TODO Check copied file now exists locally

}



UNIFI_VIDEO_BACKUP_CONFIG_LOCAL () {
	
	# Function to copy todays auto config backup from a local installation of UniFi Video back to another local directory
	
	# Required variables:
	# MACHINE_DEFAULT_USER
	
	# Input arguments:
	# $1 = UniFi Video description (lowercase, no spaces)
	# $2 = Directory to store the backup config files (no trailing slash)
	
	# Example usage:
	# UNIFI_VIDEO_BACKUP_CONFIG_LOCAL "devicename" "/path/to/unifi-video/config-backup"
	
	local UNIFI_VIDEO_NICKNAME=${1}
	local STORAGE_DIRECTORY=${2}
	
	local BACKUP_FILENAME="unifi-video_${UNIFI_VIDEO_NICKNAME}_config_backup_$(date +"%F")_$(date +"%H-%M-%S").zip"
	
	if [[ ! -z "${MACHINE_DEFAULT_USER}" ]]; then 
		local NEW_OWNER=${MACHINE_DEFAULT_USER}
	fi
	
	# TODO Check download directory exists
	
	# Find and copy the config backup
	sudo find "/usr/lib/unifi-video/data/backup/autobackup" -name "$(date +"%F")*.zip" -exec sudo /bin/cp '{}' "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}" \;
	
	echo "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
	
	# Change owner and permissions, if the newly-created file exists
	if [[ -f "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}" ]] && [[ ! -z "${NEW_OWNER}" ]]; then
		
		/usr/bin/sudo /bin/chown "${NEW_OWNER}:${NEW_OWNER}" "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
	
		/usr/bin/sudo /bin/chmod 775 "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
		
	fi
	
	# TODO Error/alert if the file doesn't exist

}

UNIFI_VIDEO_BACKUP_CONFIG_REMOTE () {
	
	# Function to copy todays auto config backup from a remote installation of UniFi Video to a local directory
	
	# Prerequisites:
	# By default, the auto config backups are owned by root, and cannot be accessed by standard users. The remote machine should be regularly changing permissions on the files (or making accessible copies elsewhere) to make sure they can be copied remotely.
	# Example change of ownership and permissions for cron:
	# */2 23 * * * sudo chown user:user -R /usr/lib/unifi-video/data/backup/autobackup/ && sudo chmod 775 -R /usr/lib/unifi-video/data/backup/autobackup/
	
	# Input arguments:
	# $1 = The user on the remote machine, used for SSH.
	# $2 = IP address of remote machine
	# $3 = Port number for remote machine SSH (defaults to 22)
	# $4 = UniFi Video description (lowercase, no spaces)
	# $5 = Directory to store the backup config files (no trailing slash)
	
	# Example usage:
	# UNIFI_VIDEO_BACKUP_CONFIG_REMOTE "user" "192.168.1.X" "22" "devicename" "/path/to/unifi-video/config-backup"
	
	local REMOTE_USER="${1}"
	local DEVICE_IP="${2}"
	local PORT_NUMBER=${3}
	local UNIFI_VIDEO_NICKNAME=${4}
	local STORAGE_DIRECTORY=${5}
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	local BACKUP_FILENAME="unifi-video_${UNIFI_VIDEO_NICKNAME}_config_backup_$(date +"%F")_$(date +"%H-%M-%S").zip"
	local NEW_OWNER=$(who am i | awk '{print $1}')
	
	
	# TODO Check download directory exists
	
	# Find and copy the config backup
	scp -P "${PORT_NUMBER}" "${REMOTE_USER}"@"${DEVICE_IP}":"/usr/lib/unifi-video/data/backup/autobackup/$(date +"%F")\*.zip" "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
	
	# Change owner and permissions, if the newly-created file exists
	if [[ -f "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}" ]]; then 
		
		sudo chown "${NEW_OWNER}:${NEW_OWNER}" "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
	
		sudo chmod 775 "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}"
		
	fi
	
	# TODO Error/alert if the file doesn't exist

}



UNIFI_CONTROLLER_BACKUP_CONFIG_LOCAL () {
	
	# Function to copy todays auto config backup from a local installation of UniFi Controller back to another local directory
	
	# Input arguments:
	# $1 = UniFi Controller description (lowercase, no spaces)
	# $2 = Directory to store the backup config files (no trailing slash)
	
	# Example usage:
	# UNIFI_CONTROLLER_BACKUP_CONFIG_LOCAL "devicename" "/path/to/unifi-video/config-backup"
	
	# Recommendations:
	# When setting a cron job to run this backup regularly, check the day(s) and frequency UniFi is already using to create its autobackups. Then base the cron schedule to run just after that.
	
	local UNIFI_CONTROLLER_NICKNAME=${1}
	local STORAGE_DIRECTORY=${2}
	
	local BACKUP_FILENAME="unifi-controller_${UNIFI_CONTROLLER_NICKNAME}_config_backup_$(date +"%F")_$(date +"%H-%M-%S").unf"
	local NEW_OWNER=$(who am i | awk '{print $1}')
	
	
	# TODO Check download directory exists
	
	# Find and copy the config backup
	#sudo find "/usr/lib/unifi/data/backup/autobackup" -name "*$(date +\%Y\%m\%d)*.unf" -exec sudo /bin/cp '{}' "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}" \;
	
	IFS=
	
	# Find all autobackup
	FIND_FILES="$(sudo find "/usr/lib/unifi/data/backup/autobackup" -name "*.unf")"
	
	# If relevant files found
	if [[ ! -z "${FIND_FILES}" ]]; then
			
		# Sort list of files
		SORTED_FILES=$(echo ${FIND_FILES} | sort -n)
		
		# Get the last file in the list
		LAST_FILE_PATH=$(echo ${SORTED_FILES} | tail -1)
		
		# Make a variable for just the filename
		LAST_FILE="$(basename ${LAST_FILE_PATH})"
		
		# Extract the version of the backup
		VERSION_OF_BACKUP=$(echo ${LAST_FILE} | cut -d_ -f2)
		
		# Extract the date of the backup
		DATE_OF_BACKUP=$(echo ${LAST_FILE} | cut -d_ -f3)
		
		# Create a varible to standardise the date
		STANDARDISED_DATE_OF_BACKUP="$(date -d "${DATE_OF_BACKUP}" +%Y-%m-%d)"
		
		# Copy the file
		sudo /bin/cp "${LAST_FILE_PATH}" "${STORAGE_DIRECTORY}/"
		
		# Change owner and permissions, if the newly-created file exists
		if [[ -f "${STORAGE_DIRECTORY}/${LAST_FILE}" ]]; then 
			
			sudo chown "${NEW_OWNER}:${NEW_OWNER}" "${STORAGE_DIRECTORY}/${LAST_FILE}"
		
			sudo chmod 775 "${STORAGE_DIRECTORY}/${LAST_FILE}"
			
		else
			
			echo "File not found"
			
			# TODO Add alert
			
		fi
		
	else
		
		echo "No relevant files found"
		
		# TODO Add alert
	
	fi #Find
	
}



UNIFI_CLOUD_KEY_BACKUP_CONFIG_REMOTE () {
	
	# Function to copy todays auto config backup from a local installation of UniFi Controller back to another local directory
	
	# Input arguments:
	# $1 = IP address of UniFi Cloud Key
	# $2 = Port number for remote machine SSH (defaults to 22)
	# $3 = UniFi Controller description (lowercase, no spaces)
	# $4 = Directory to store the backup config files (no trailing slash)
	
	# Example usage:
	# UNIFI_CONTROLLER_BACKUP_CONFIG_LOCAL "devicename" "/path/to/unifi-video/config-backup"
	
	# Recommendations:
	# When setting a cron job to run this backup regularly, check the day(s) and frequency UniFi is already using to create its autobackups. Then base the cron schedule to run just after that.
	
	local DEVICE_IP="${1}"
	local PORT_NUMBER=${2}
	local UNIFI_CONTROLLER_NICKNAME=${3}
	local STORAGE_DIRECTORY=${4}
	
	# Establish port number
	if [[ ! -z "${PORT_NUMBER}" ]]; then
		local PORT_NUMBER="${PORT_NUMBER}"
	else
		local PORT_NUMBER=22
	fi
	
	echo "DEVICE_IP:   ${DEVICE_IP}"
	echo "PORT_NUMBER: ${PORT_NUMBER}"
	echo "UNIFI_CONTROLLER_NICKNAME: ${UNIFI_CONTROLLER_NICKNAME}"
	echo "STORAGE_DIRECTORY: ${STORAGE_DIRECTORY}"
	
	local BACKUP_FILENAME="unifi-controller_${UNIFI_CONTROLLER_NICKNAME}_config_backup_$(date +"%F")_$(date +"%H-%M-%S").unf"
	local NEW_OWNER=$(who am i | awk '{print $1}')
	
	
	# TODO Check download directory exists
	
	# Find and copy the config backup
	#sudo find "/usr/lib/unifi/data/backup/autobackup" -name "*$(date +\%Y\%m\%d)*.unf" -exec sudo /bin/cp '{}' "${STORAGE_DIRECTORY}/${BACKUP_FILENAME}" \;
	
	IFS=
	
	# Find all autobackup
	FIND_FILES="$(ssh root@"${DEVICE_IP}" -p "${PORT_NUMBER}" 'sudo find "/data/autobackup" -name "*.unf"')"
	echo "${FIND_FILES}"
	
	# If relevant files found
	if [[ ! -z "${FIND_FILES}" ]]; then
			
		# Sort list of files
		SORTED_FILES=$(echo ${FIND_FILES} | sort -n)
		
		# Get the last file in the list
		LAST_FILE_PATH=$(echo ${SORTED_FILES} | tail -1)
		
		# Make a variable for just the filename
		LAST_FILE="$(basename ${LAST_FILE_PATH})"
		
		# Extract the version of the backup
		VERSION_OF_BACKUP=$(echo ${LAST_FILE} | cut -d_ -f2)
		
		# Extract the date of the backup
		DATE_OF_BACKUP=$(echo ${LAST_FILE} | cut -d_ -f3)
		
		# Create a varible to standardise the date
		STANDARDISED_DATE_OF_BACKUP="$(date -d "${DATE_OF_BACKUP}" +%Y-%m-%d)"
		
		# Copy the file
		scp -P "${PORT_NUMBER}" root@"${DEVICE_IP}":"${LAST_FILE_PATH}" "${STORAGE_DIRECTORY}/"
		
		# Change owner and permissions, if the newly-created file exists
		if [[ -f "${STORAGE_DIRECTORY}/${LAST_FILE}" ]]; then 
						
			chown "${NEW_OWNER}:${NEW_OWNER}" "${STORAGE_DIRECTORY}/${LAST_FILE}"
		
			chmod 775 "${STORAGE_DIRECTORY}/${LAST_FILE}"
			
		else
			
			echo "File not found"
			
			# TODO Add alert
			
		fi
		
	else
		
		echo "No relevant files found"
		
		# TODO Add alert
	
	fi #Find
	
}
