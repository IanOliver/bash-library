#!/usr/bin/env bash


TIMESTAMP () {
	
	# Function to output a date/time in a preferred format
	
	# See https://www.man7.org/linux/man-pages/man1/date.1.html
	
	# See https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
	
	# Number padding https://stackoverflow.com/a/54066479
	# By default, date pads numeric fields with zeroes. The following optional flags may follow '%':
	# - (hyphen) do not pad the field
	# _ (underscore) pad with spaces
	# 0 (zero) pad with zeros
	# ^ use upper case if possible
	# # use opposite case if possible
	
	# Input arguments:
	# $1 = Preset required (e.g. DATE_NOW, TIME_NOW_FULL, EPOCH)
	
	# Example usage:
	# VARIABLE="$(TIMESTAMP DATE_NOW)"
	# echo "$(TIMESTAMP DATE_NOW)"
	
	if [[ "${1}" = "DATE_NOW" ]]; then
	
		local VALUE=$(date +"%F") # Today's date as 1980-12-31
		
	elif [[ "${1}" = "DATE_TOMORROW" ]]; then
	
		local VALUE=$(date "--date=+1 days" +"%F") # Tomorrow's date as 1980-12-31
		
	elif [[ "${1}" = "DATE_YESTERDAY" ]]; then
	
		local VALUE=$(date "--date=-1 days" +"%F") # Yesterday's date as 1980-12-31
	
	# Day names
		
	elif [[ "${1}" = "DAY_NAME_NOW" ]]; then
	
		local VALUE="$(date +"%A")" # Today's day name as "Monday"
		
	elif [[ "${1}" = "DAY_NAME_TOMORROW" ]]; then
	
		local VALUE="$(date "--date=+1 days" +"%A")" # Tomorrow's day name as "Tuesday"
		
	elif [[ "${1}" = "DAY_NAME_YESTERDAY" ]]; then
	
		local VALUE="$(date "--date=-1 days" +"%A")" # Yesterday's day name as "Sunday"
		
	# Months
	
	elif [[ "${1}" = "MONTH_NAME_NOW" ]]; then
	
		local VALUE="$(date +"%B")" # Today's month name as "January"
	
	elif [[ "${1}" = "MONTH_NAME_ABBR_NOW" ]]; then
	
		local VALUE="$(date +"%b")" # Today's month name as "Jan"
		
	# Day number of week/month/year
		
	elif [[ "${1}" = "DAY_NUMBER_OF_WEEK_NOW" ]]; then
				
		local VALUE=$(date +"%u") # 1, 2, 7
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_NOW" ]]; then
		
		local VALUE=$(date +%-d) # 1, 14, 31 (not padded, e.g. "1", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_ZEROED_NOW" ]]; then
		
		local VALUE=$(date +%d) # 1, 14, 31 (is zero padded, e.g. "01", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_SPACED_NOW" ]]; then
		
		local VALUE=$(date +%_d) # 1, 14, 31 (is space padded, e.g. " 1", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_YESTERDAY" ]]; then
		
		local VALUE=$(date "--date=-1 days" +%-d) # Yesterday's date as 1, 14, 31 (not padded, e.g. "1", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_ZEROED_YESTERDAY" ]]; then
		
		local VALUE=$(date "--date=-1 days" +%d) # Yesterday's date as 1, 14, 31 (is zero padded, e.g. "01", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_MONTH_SPACED_YESTERDAY" ]]; then
		
		local VALUE=$(date "--date=-1 days" +%_d) # Yesterday's date as 1, 14, 31 (is space padded, e.g. " 1", "30")
		
	elif [[ "${1}" = "DAY_NUMBER_OF_YEAR_NOW" ]]; then
		
		local VALUE=$(date +%j) # 001, 365, 366
		
	# Week number of month/year
		
	elif [[ "${1}" = "WEEK_NUMBER_OF_YEAR_NOW" ]]; then
		
		local VALUE=$(date +"%V") # 01, 20, 52
		
	elif [[ "${1}" = "WEEK_NUMBER_OF_MONTH_NOW" ]]; then
		
		local VALUE=$((($(date +%-d)-1)/7+1)) # 1, 2, 3, 4, 5
		# https://serverfault.com/questions/383666/how-to-determine-number-of-week-of-the-month
		
	# Time
		
	elif [[ "${1}" = "HOUR_NOW" ]]; then
		
		local VALUE=$(date +"%H") # 07, 13, 23
		
	elif [[ "${1}" = "MINUTE_NOW" ]]; then
		
		local VALUE=$(date +"%M") # 00, 25, 59
		
	elif [[ "${1}" = "SECOND_NOW" ]]; then
		
		local VALUE=$(date +"%S") # 00, 25, 59
		
	elif [[ "${1}" = "TIME_NOW_FULL" ]]; then
		
		local VALUE=$(date +"%T") # 07:15:36, 13:58:45, 23:59:59
		
	elif [[ "${1}" = "TIME_NOW_SHORT" ]]; then
		
		local VALUE=$(date +"%R") # 07:15, 13:58, 23:59
		
	elif [[ "${1}" = "TIME_NOW_FULL_FILENAME" ]]; then
		
		local VALUE=$(date +"%H-%M-%S") # 07-15-36, 13-58-45, 23-59-59
		
	elif [[ "${1}" = "TIME_NOW_SHORT_FILENAME" ]]; then
		
		local VALUE=$(date +"%H-%M") # 07-15, 13-58, 23-59
		
	elif [[ "${1}" = "EPOCH" ]]; then
		
		local VALUE=$(date +"%s") # Seconds since 1970-01-01
		
	fi
	
	echo "${VALUE}"
	
}
