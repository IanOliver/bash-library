#!/usr/bin/env bash

MAILGUN () {
	
	# See https://documentation.mailgun.com/user_manual.html#sending-via-api
 	# See https://documentation.mailgun.com/api-sending.html#sending
	
	# Usage (to only, text only):
	# MAILGUN	"To Name" "to@domain.com" "From Name" "from@domain.com" "" "" "" "" "Subject" "Text content" "" "tag-name"
	
	# Usage (with CC and BCC, text and HTML):
	# MAILGUN	"To Name" "to@domain.com" "From Name" "from@domain.com" "CC Name" "cc@domain.com" "BCC Name" "bcc@domain.com" "Subject" "Text content" "<html>HTML version of the body</html>" "tag-name"
	
	# To Name           : $1
	# To Address        : $2
	
	# From Name         : $3
	# From Address      : $4
	
	# CC Name           : $5
	# CC Address        : $6
	
	# BCC Name          : $7
	# BCC Address       : $8
	
	# Subject           : $9
	# Text Content      : $10
	# HTML Content      : $11
	
	# Tag               : $12
	
	# Campaign          : $13
	# Delivery Time     : $14
	# Test Mode         : $15
	# Tracking          : $16
	# Tracking Clicks   : $17
	# Tracking Opens    : $18
	# Reply To          : $19
	# Variable String   : $20
	# Sending Domain    : $21
	# API Key           : $22
	# API Base URL      : $23
	
	
	
	# cURL command and auth
	local MAILGUN_CURL_STRING="-s --user "api:"${MAILGUN_API_KEY}"""

	# URL
	local MAILGUN_CURL_STRING+=" ${MAILGUN_URL}/messages"

	# To
	local MAILGUN_CURL_STRING+=" --data-urlencode to='${1} <${2}>'"

	# From
	local MAILGUN_CURL_STRING+=" --data-urlencode from='${3} <${4}>'"

	# Subject
	local MAILGUN_CURL_STRING+=" --data-urlencode subject='${9}'"

	# CC
	if [ ! -z "${6}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode cc='${5} <${6}>'"
	fi

	# BCC
	if [ ! -z "${8}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode bcc='${7} <${8}>'"
	fi

	# Text content
	if [ ! -z "${10}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode text='${10}'"
	fi

	# HTML content
	if [ ! -z "${11}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode html='${11}'"
	fi

	# Tag(s)
	# A single message may be marked with up to 3 tags.
	# Tags are case insensitive and should be ascii only. Maximum tag length is 128 characters.
	# Multiple tags can be passed to the function like so: "tag-1,tag-2,tag-3"
	# TODO: Implement a for loop to include an array of tags
	if [ ! -z "${12}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tag='${12}'"
	fi

	# Campaign
	if [ ! -z "${13}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:campaign='${13}'"
	fi

	# Delivery Time
	# Messages can be scheduled for a maximum of 3 days in the future.
	# JSON does not have a built-in date type, dates are passed as strings encoded according to RFC 2822#page-14. This format is native to JavaScript and is also supported by most programming languages out of the box:
	# https://tools.ietf.org/html/rfc2822.html#page-14
	if [ ! -z "${14}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:deliverytime='${14}'"
	fi

	# Test Mode
	# Enables sending in test mode. Pass yes if needed.
	# Mailgun will accept the message but will not send it.
	# You are charged for messages sent in test mode.
	if [ "${15}" = "yes" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:testmode=yes"
	fi

	# Tracking
	# Toggles tracking on a per-message basis. Pass yes or no.
	if [ "${16}" = "yes" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking=yes"
	elif [ "${16}" = "no" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking=no"
	fi

	# Click Tracking
	# Toggles clicks tracking on a per-message basis.
	# Has higher priority than domain-level setting. Pass yes, no or htmlonly.
	if [ "${17}" = "yes" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking-clicks=yes"
	elif [ "${17}" = "no" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking-clicks=no"
	elif [ "${17}" = "htmlonly" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking-clicks=htmlonly"
	fi

	# Tracking Opens
	# Toggles opens tracking on a per-message basis.
	# Has higher priority than domain-level setting. Pass yes or no.
	if [ "${18}" = "yes" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking-opens=yes"
	elif [ "${18}" = "no" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode o:tracking-opens=no"
	fi
	
	# Reply-To
	if [ ! -z "${19}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode h:Reply-To='${19}'"
	fi
	
	# Custom Variables
	# To be passed through to this function as a string:
	# name1=value1,name2=value2,name3=value3
	# This then gets split and posted
	# TODO: Implement a for loop to include an array of variables
	if [ ! -z "${20}" ]; then
		local MAILGUN_CURL_STRING+=" --data-urlencode v:'${20}'"
	fi
	
	# Run cURL
	echo "curl ${MAILGUN_CURL_STRING}" | sh
	
	
}



PUSHOVER () {
	
	# USAGE
	# Basic:
	# PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "Title" "Message text"
	
	# Emergency priority (must be acknowledged):
	# PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "Title" "Message text" "2" "" "" "" ""
	
	# High priority:
	# PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "Title" "Message text" "1" "" "" "" ""
	
	# Low priority:
	# PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "Title" "Message text" "-1" "" "" "" ""
	
	# With a sound:
	# PUSHOVER "${PUSHOVER_USER}" "${PUSHOVER_TOKEN}" "Title" "Message text" "" "cashregister" "" "" ""
	
	local PUSHOVER_API_URL="https://api.pushover.net/1/messages.json"
	# See https://pushover.net/api
	
	# Priority:
	# Send as -2 to generate no notification/alert, -1 to always send as a quiet notification, 1 to display as high-priority and bypass the user's quiet hours, or 2 to also require confirmation from the user. 0 is a default notification.
	
	local API_USER="${1}"
	local API_TOKEN="${2}"
	local TITLE="${3}"
	local MESSAGE="${4}"
	local PRIORITY="${5}"
	local SOUND="${6}"
	local IMAGE_PATH="${7}"
	local RETRY="${8}"
	local EXPIRE="${9}"
	
	
	if [[ "${PRIORITY}" = "2" ]] && [[ ! -z "${RETRY}" ]]; then
		local RETRY=${RETRY}
	elif [[ "${PRIORITY}" = "2" ]] && [[ -z "${RETRY}" ]]; then
		local RETRY=60
	fi
	
	if [[ "${PRIORITY}" = "2" ]] && [[ ! -z "${EXPIRE}" ]]; then
		local EXPIRE=${EXPIRE}
	elif [[ "${PRIORITY}" = "2" ]] && [[ -z "${EXPIRE}" ]]; then
		local EXPIRE=10800
	fi
	
	# cURL arguments
	# -s silent, no traffic or progress meter output
	local PUSHOVER_CURL_STRING="-s "

	local PUSHOVER_CURL_STRING+="--form-string \"user=${API_USER}\" "
	local PUSHOVER_CURL_STRING+="--form-string \"token=${API_TOKEN}\" "
	
	if [[ ! -z "${TITLE}" ]]; then
		local PUSHOVER_CURL_STRING+="--form-string \"title=${TITLE}\" "
	fi
		
	local PUSHOVER_CURL_STRING+="--form-string \"message=${MESSAGE}\" "
	
	if [[ ! -z "${PRIORITY}" ]]; then
		local PUSHOVER_CURL_STRING+="--form-string \"priority=${PRIORITY}\" "
	fi
	
	if [[ "${PRIORITY}" = "2" ]]; then
		local PUSHOVER_CURL_STRING+="--form-string \"retry=${RETRY}\" "
		local PUSHOVER_CURL_STRING+="--form-string \"expire=${EXPIRE}\" "
	fi
	
	if [[ ! -z "${SOUND}" ]]; then
		local PUSHOVER_CURL_STRING+="--form-string \"sound=${SOUND}\" "
	fi
	
	if [[ ! -z "${IMAGE_PATH}" ]]; then
		local PUSHOVER_CURL_STRING+="-F \"attachment=@${IMAGE_PATH}\" "
	fi
	
	local PUSHOVER_CURL_STRING+="--form-string \"timestamp=$(TIMESTAMP EPOCH)\" "
	
	# Run cURL
	echo "curl ${PUSHOVER_CURL_STRING} ${PUSHOVER_API_URL}" | bash
	
	# Add a linebreak
	echo ""
	
}



TWILIO () {
	
	# Function to send an SMS message with Twilio
	
	# Required variables:
	# TWILIO_ACCOUNT_SID
	# TWILIO_AUTH_TOKEN
	# TWILIO_SENDING_NUMBER
	
	# Input arguments:
	# $1 = Twilio account SID
	# $2 = Twilio auth token
	# $3 = Twilio From number
	# $4 = To number
	# $5 = Message
	
	# See https://www.twilio.com/docs/api/messaging
	
	# USAGE
	# TWILIO "${TWILIO_ACCOUNT_SID}" "${TWILIO_AUTH_TOKEN}" "${TWILIO_SENDING_NUMBER}" "+447000000000" "Message text"
	
	local ACCOUNT_SID="${1}"
	local AUTH_TOKEN="${2}"
	local FROM_NUMBER="${3}"
	local TO_NUMBER="${4}"
	local MESSAGE="${5}"
	
	local TWILIO_API_URL="https://api.twilio.com/2010-04-01/Accounts/${ACCOUNT_SID}/Messages.json"
	
	# cURL arguments
	# -s silent, no traffic or progress meter output
	#CURL_STRING="-s "

	local TWILIO_CURL_STRING+="-X POST "

	local TWILIO_CURL_STRING+="-u '${ACCOUNT_SID}:${AUTH_TOKEN}' "
	local TWILIO_CURL_STRING+="--data-urlencode \"From=${FROM_NUMBER}\" "
	local TWILIO_CURL_STRING+="--data-urlencode \"To=${TO_NUMBER}\" "
	local TWILIO_CURL_STRING+="--data-urlencode \"Body=${MESSAGE}\" "
		
	# Run cURL
	echo "curl ${TWILIO_CURL_STRING} ${TWILIO_API_URL}" | bash
	
	# Add a linebreak
	echo ""
	
}
