#!/usr/bin/env bash

HD_TEMP () {

	# Function to get the temperature of a hard drive.

	# Requires hddtemp to be installed
	
	# Expected output:
	# Either a human-readable temperature or raw number
	
	# Input arguments:
	# $1 = Drive path
	# $2 = Format to output the number [Blank = default with '°C' included, "number" = pure number]
	
	# Example usage:
	# HD_TEMP "/dev/sda"
	# VARIABLE="$(HD_TEMP "/dev/sda")"
	# VARIABLE="$(HD_TEMP "/dev/sda" "raw")"

	if [[ "${2}" = "number" ]]; then

		sudo hddtemp "${1}" | awk '{print $3}' | sed 's/\°C//g'

	else

		sudo hddtemp "${1}" | awk '{print $3}'

	fi

}
