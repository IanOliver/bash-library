#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"


TO_NAME="${MAILGUN_DEFAULT_RECEIVING_NAME}"
TO_ADDRESS="${MAILGUN_DEFAULT_RECEIVING_ADDRESS}"

FROM_NAME="${MAILGUN_DEFAULT_SENDING_NAME}"
FROM_ADDRESS="${MAILGUN_DEFAULT_SENDING_ADDRESS}"

SUBJECT="Testing Mailgun Function"

BODY_TEXT="This is just a short email to test Mailgun on ${MACHINE_NAME}."


MAILGUN "${TO_NAME}" "${TO_ADDRESS}" "${FROM_NAME}" "${FROM_ADDRESS}" "" "" "" "" "${SUBJECT}" "${BODY_TEXT}" "" ""
