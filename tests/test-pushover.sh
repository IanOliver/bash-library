#!/usr/bin/env bash

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

PUSHOVER "${PUSHOVER_DEFAULT_USER}" "${PUSHOVER_DEFAULT_TOKEN}" "Test ${MACHINE_NAME} Pushover" "Test message body"
