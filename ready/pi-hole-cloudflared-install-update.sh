#!/usr/bin/env bash

# Source profile variables
. "/etc/profile"

# Source machine-specific variables
. "${MACHINE_SCRIPTS_DIR}/this-machine.sh"

# Source bash-library
. "${MACHINE_SCRIPTS_DIR}/bash-library/library.sh"

OPERATION="${MACHINE_NAME} Installing/Updating cloudflared"

PI_HOLE_CLOUDFLARED_INSTALL_UPDATE
