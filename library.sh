#!/usr/bin/env bash

# Environment variables to be set in .bashrc:
# MACHINE_SCRIPTS_DIR (e.g. "/storage/scripts")

# Machine-specific variables to be set in this-machine.sh:
# MACHINE_NAME
# MACHINE_DEFAULT_USER (e.g. "bob")
# PFSENSE_CERT_USER (e.g. "admin")
# PFSENSE_IP (e.g. "192.168.1.1")
# SYNCTHING_API_KEY
# PUSHOVER_DEFAULT_USER
# PUSHOVER_DEFAULT_TOKEN


# Get the relative path this file
BASH_LIBRARY_DIR_RELATIVE="$(dirname ${BASH_SOURCE[0]})"

# Load modules
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/backup.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/certificates.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/communication.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/date-time.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/dns.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/drive-usage.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/health.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/machine.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/maths.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/paths.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/pi-hole.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/software.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/ssh.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/user.sh"
. "${BASH_LIBRARY_DIR_RELATIVE}/modules/vehicles.sh"

# For outgoing SSH commands that will be run with cron, we need to use keychain
if [[ -d  "/home/${MACHINE_DEFAULT_USER}/.keychain" ]] && [[ ! -z "${MACHINE_DEFAULT_USER}" ]]; then
	. "/home/${MACHINE_DEFAULT_USER}/.keychain/$HOSTNAME-sh"
elif [[ -d  "$HOME/.keychain" ]]; then
	. "$HOME/.keychain/$HOSTNAME-sh"
fi


REQUIRE_ROOT () {
	echo "Root required..."
	sudo echo "Root confirmed..."
}


FAIL_NOTIFY () {
	PUSHOVER "${PUSHOVER_IAN_USER}" "${PUSHOVER_IAN_TOKEN}" "" "FAILED ${OPERATION} ${STAGE}"
	exit 1
}


